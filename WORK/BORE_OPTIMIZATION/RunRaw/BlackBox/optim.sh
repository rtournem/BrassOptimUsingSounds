#!/bin/bash

#table rase de ce qui pourrait exister
./killName matlab_server
./killName sgtelib.exe
./killName nomad
./killName MATLAB
./killName matlab_helper

sleep 10

DATAPATH=DATA/optim/JASA2018/
CODEPATH=WORK/BORE_OPTIMISATION/RunRaw/BlackBox/
TOOLBOXPATH=TOOLBOXES/nomad/

cd ../../../../$DATAPATH

#création du serveur (s'il n'existe pas encore)
#mkdir server$1
#création du dossier résultat ainsi que du dossier contenant les vecteurs sons estimés par un modèle pour l'équilibrage harmonique
#rm -r server$1/*
mkdir -p server${1}/repe${2}/tmp
mkdir -p server${1}/repe${2}/Results

cd ../../../$CODEPATH

# copie de tous les fichiers utiles au server et renommage pour avoir une syntaxe standard
cp bb_$1.m matlab_server.m matlab_server.sh algo_$1.txt x_$1.txt killName ../../../../${DATAPATH}server${1}/repe${2}

#on descend dans le sous-dossier du bon server et de la bonne répé
cd ../../../../${DATAPATH}server${1}/repe${2}
mv bb_$1.m bb.m
mv algo_$1.txt algo.txt
mv x_$1.txt x.txt

mkdir -p nomad/{ext/sgtelib,lib,bin}
cp -r ../../../../../${TOOLBOXPATH}/install ../../../../../${TOOLBOXPATH}/src ../../../../../${TOOLBOXPATH}/tools ../../../../../${TOOLBOXPATH}/utils  nomad/
cp -r ../../../../../${TOOLBOXPATH}/ext/sgtelib/src nomad/ext/sgtelib/
cp  ../../../../../${TOOLBOXPATH}/setEnv nomad/

cd nomad/install/
# this was a necessary step on my HPC system, this will differ on yours. Basically, I had to find a gcc version that could compile without problme nomad and sgtelib
module unload compiler/gcc/6.2.0
module load compiler/gcc/4.8.4
./install.sh > LogInstallNomad
cd ../
. setEnv
cd ../
pwd | xargs echo "we are going to work in  the folder "
module load tools/matlab/R2015a

echo repetition $2 du cas $1

#L'UNIQUE FORMULE permettant de lancer Matlab en arrière plan en toute sérénité!
matlab -nodesktop -nosplash -nodisplay < matlab_server.m > LogMatlab &
echo matlab_server launched
sleep 60
#on lance nomad
nomad algo.txt > LogNomad &

a=0
#attention code optim sh seulement valable pour les cas à 200 répétitions, sinon, il faut remettre le 170 à 95
while [ $a -lt 75 ];
do
		a=`eval ls -1 Results/ | wc -l`
		echo $a
		sleep 3600
done

wait
echo nomad finished
sleep 5
