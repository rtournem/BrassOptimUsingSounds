#!/bin/bash

./killName matlab_server
./killName sgtelib_server.exe
./killName nomad
./killName MATLAB

for DESTINATION in server1 server2 server3 server4 server5 server6 server7 server8 server9 server10 server11 server12
do
     rm -r $DESTINATION/*
done
