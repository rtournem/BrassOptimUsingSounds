# Dependency

This optimization procedure is based on the surrogate-assisted nomad
optimizer. You can download the source code at this
[link](https://www.gerad.ca/nomad/) and place it under the folder
`TOOLBOXES/nomad`. You can compile it in the folder to test it, but
the script optim.sh will do it for you when you will use the complete
procedure.

# General Considerations

In order to be able to use this optimization procedure, you will need
to have some knowledge in bash scripting, in matlab and SLURM.

The script `optim.sh` in the folder
`WORK/BORE_OPTIMIZATION/RunRaw/BlackBox` is a good example of how to
make the code work. It works following these steps:

1. create a folder structure where to run the codes and store the
   results,
2. install nomad and sgtelib
3. run the file `matlab_server.m` which will wait for the optimization
   to start
4. run the nomad command and the associated setup file.

the setup file is telling nomad to execute the bash script
`matlab_server.sh` which will discuss with `matlab_server.m` using
flag files.

In terms of design problems, you can create new problems tweaking the
file `OPTProbDef.m` in the folder `WORK/BORE_OPTIMIZATION/Ressources`.

# Reproducing the JASA 2019 results

In order to reproduce the results from this article you can directly
run `$ .\optim.sh X Y`: X being the studied case between 1 and
12 (see the description of file `bb_X.m` in the same folder), and Y an
arbitrary number used to make repetition of the same optimization
task. (To test quickly optim.sh X 1 is perfect)

You can also reproduce the analysis made for the article, folder
`WORK/BORE_OPTIMIZATION/ResultsAnalysis`. You will just have to launch
the file `FiguresArticle.m` and choose a corresponding value for the
variable `fig2plot`

# Objective function surface for a 2D design problem

Regarding the optimization I also left the possibility to produce the
objective functions surfaces for a 2D design problem. For this task
you can run the file
`WORK/BORE_OPTIMIZATION/BBoxDisc/DiscretizationTest.m`
