function OPTProb=OPTProbDef(Problem)
%manages the definition of the design problem

%Problem possibilities:
%-inharmpm (global or not depending on the fingering(s) selected)
%-inharmplasticity
%-cgspm  (evolution of cgs according to one PM) REMARK: we only can and
%want to optimize one pm optimizing every pm together is a very hard task.
%-cgshomo
%-cgsplasticity
%REMARK:
%-If you wanna run 'inharmplasticity','cgsplasticity' at the same time
%(obj+cst) you have to keep the Pms separations! 

%Foundamental: problem size: 4fing 8reg;
OPTProb.SimuSize = [4, 8];
OPTProb.Nature = Problem.name; 
OPTProb.NatureType = Problem.catalyst; 
% OPTProb.SurType='LPMPD'; % preset to load in surrogate_server.cpp
%choose a certain to make your zone thresholds and initsols . Then tagging 
%the files you can retrieve them with the parameter NatureType
global INSTRUS
INSTRUS = {'D0','D1','D2','D23'};
OPTProb.INSTRUS = INSTRUS;
% Problem='inharmplasticity';
disp(Problem);
OPTProb.fastConv = 1; %as soons as a solution is found->end of search (it can be a "not that good sol" %TO BE CORRECTED IF WANTED AGAIN FOR DEBUGGING SINCE THE NEW MASKMANAGER POINT OF VIEW
OPTProb.SystematicTry = 2;%0: only centroid point + neighboring points 1: only systemic search with Fplay moving 2:both
OPTProb.plot = 0;
OPTProb.Workers = 1;
OPTProb.PmZone = 45 * OPTProb.Workers;%number of point sampled in the criteria which just need Pm slices
OPTProb.verboseSgtelib = 0;

% The variable Problem select one of the following optimization problem
switch Problem.crit
	case 'inharmpm' %setting harmonic of reference (number 4 most of hte time)
		OPTProb.desc={'inharmpm',[5000 9000],{4},[];%1:descriptor name, 2:pms limits, 3:possible settings 4:H threshold for constraint, [] if objective
			};
		OPTProb.reg=2:5;
		OPTProb.fing=1;%1:D0 2:D1 3:D2 4:D23
	case 'inharmpmundercgs'
		OPTProb.desc={'inharmpm',[5000 9000],{4},[];%1:descriptor name, 2:pms limits, 3:possible settings 4:H threshold for constraint, [] if objective
			'cgspm',[5000 9000],{},2.08;};
		OPTProb.reg=[2,3,4,5];
		OPTProb.fing=1;%1:D0 2:D1 3:D2 4:D23
	case 'cgspmunderinharm' %A FINIR en temps voulu
		OPTProb.desc={'cgspm',[5000 9000],{},[];%1:descriptor name, 2:pms limits, 3:possible settings 4:H threshold for constraint, [] if objective
			'inharmpm',[5000 9000],{4},-12;};
		OPTProb.reg=[2,3,4,5];
		OPTProb.fing=1;%1:D0 2:D1 3:D2 4:D23
	case 'inharminstru'
		OPTProb.desc={'inharmpm',[6000 9000],{},[]};
		OPTProb.reg=2:6;
		OPTProb.fing=1:4;
	case 'inharmplasticity'
		OPTProb.desc={'inharmplasticity',[1000,5000;8000,12000],{},[]}; %OPTProb.pm is not used for this problem since we want to see the evolution of inharm with pm.
		OPTProb.reg=2:5;
		OPTProb.fing=1;%1: D0 2:D1 3:D2 4:D23
	case COUPLES %i have to implement a trick in simuHB because we want to know the inharmonicity
		OPTProb.desc={Problem,[6000,9000],{},[]};%Problem should have a couple name
	case 'cgspm'
		OPTProb.desc={'cgspm',[5000 9000],{},[]};
		OPTProb.reg=[2,3,4,5];
		OPTProb.fing=1;
	case 'cgshomo'
		OPTProb.desc={'cgshomo',[6000 9000],{},[]}; %check cgs homogeneity between notes and fingerings.
		OPTProb.reg=[2,3,4,5];
		OPTProb.fing=1;
	case 'cgsplasticity'
		OPTProb.desc={'cgsplasticity',[1000,5000;9000,12000],{},[]}; %OPTProb.pm is not used for this problem since we want to see the evolution of inharm with pm.
		OPTProb.reg=2:4;
		OPTProb.fing=1;
end 
disp(OPTProb);
end

