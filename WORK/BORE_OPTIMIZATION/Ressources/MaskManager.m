function [Mask,OPTProb]=MaskManager(OPTProb,SelectedNotes,PowNotes,Candidate)
%give the next mask(s) based on the data of the preprocessing and the status of
%the optimization process.

%the function handling the status of the simulation 
[Pms, Marker, OPTProb] = SimulationManagement(OPTProb, SelectedNotes, PowNotes, Candidate);
if Pms
	varMin = min(OPTProb.CloudReg);
	varMax = max(OPTProb.CloudReg);
	slope=varMax-varMin;
	STOP=0;
	masktry=0;
	while STOP==0
		Mask=varMin(1:3)+rand(1,3).*slope(1:3); 
		paramNorm = (OPTProb.CloudReg - OPTProb.minValReg) ./ (OPTProb.maxValReg - OPTProb.minValReg);
		pointNorm = ([Mask Candidate] - OPTProb.minValReg(1,:)) ./ (OPTProb.maxValReg(1,:) - OPTProb.minValReg(1,:));
		distNeigh = sort(pdist2(paramNorm, pointNorm, 'chebychev'));
		if distNeigh(OPTProb.neighborIdx) < OPTProb.neighborsEntireIdxPM(OPTProb.CurReg)
			STOP=1;
		end
		masktry=masktry+1;
		if masktry==10000
			Mask=zeros(1,3);
			disp('pas de masques convenables');
			break;
		end
	end
else
	%if at this point the function never returned anything, it means
	%we simulated enough embouchures and then: Mask=0
	Mask=zeros(1,3);
end
OPTProb.Marker=Marker;
end
