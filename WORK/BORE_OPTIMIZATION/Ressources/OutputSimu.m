function [Notes,Pow,AllNotes]=OutputSimu(OPTProb,Candidate)
%function computing simulations for the optimization process

global INSTRUS ROOT
OPTProb.INSTRUS=INSTRUS;%for parallel loop
OPTProb.ROOT=ROOT;%for parallel loop
Notes=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
Pow=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
AllNotes=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
%%%%%%%%%%%%%%%%%%%%% PARALLEL MANAGEMENT %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if OPTProb.Workers>1
	OPTProb.ROOT=[ '../../' OPTProb.ROOT];
	poolSimu=gcp('nocreate');%if no worker opened.
	if isempty(poolSimu)
		pc = parcluster('local');
		! mkdir MPCT
		pc.JobStorageLocation = [pwd '/MPCT'];
		parpool(pc, OPTProb.Workers, 'IdleTimeout', 600);
	elseif poolSimu.NumWorkers~=OPTProb.Workers %if the number of opened worker is not right 
		%or the time of execution passes two hours then because of parfoor supposed memory leak 
		%on our task we restart the pool.
		delete(gcp('nocreate'))
		pc = parcluster('local');
		! mkdir MPCT
		pc.JobStorageLocation = [pwd '/MPCT'];
		parpool(pc, OPTProb.Workers, 'IdleTimeout', 600);
		parpool(OPTProb.Workers);		
	end
	%all the parallel actions are going to take place in the special tmp folder
	%while the rest of the code is taking place in the "normal" folder!
	%A very interesting trick
	spmd
    CurDir = pwd;
		w = getCurrentWorker;
		if ~isempty(strfind(CurDir,'Worker')) %if the last geom failed
			cd('../../');
			rmdir(['tmp/Worker' num2str(w.ProcessId)],'s');
		end
		mkdir(['tmp/Worker' num2str(w.ProcessId)]);
		cd(['tmp/Worker' num2str(w.ProcessId)]);
	end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for i=OPTProb.fing
	load([ROOT 'DATA/optim/Catalyst/THRESHOLDS/Thresholds_' OPTProb.NatureType '_' INSTRUS{i}]);
	OPTProb.THRESHOLDS = Thresholds;
	OPTProb.neighborsEntire = neighborsEntire;
	OPTProb.neighborsDesign = neighborsDesign;
	OPTProb.neighborIdx = neighborIdx;
	OPTProb.minVal = minVal;
	OPTProb.maxVal = maxVal;
	OPTProb.PMS = Pms;
	load([ROOT 'DATA/optim/Catalyst/INITSOL/InitSol_' OPTProb.NatureType '_' INSTRUS{i}]);
	OPTProb.INITSOLS=InitSols;%also declared in NonfeasibleGeom!!!!
	OPTProb.MARKERS=Markers; %unique index for the sound corresponding to their idxpm/reg
	
	%checking the feasability of a candidate
	if NonfeasibleGeom(OPTProb,Candidate,0) %function managing the impossible geom and providing a stop or not according to the situation
		dispstat('bad bore', 'keepthis');
		system('./killName sgtelib.exe');
		return;
	else
		dispstat('good bore', 'keepthis');	
	end
	
	OPTProb.CurFing=i;
	[OPTProb.Mask,OPTProb.Phys,OPTProb.Instru,OPTProb.Simu,OPTProb.Optim,impedNFO]...
		= GeneralParamEdition(OPTProb,Candidate);
	[OPTProb.Instru.imped,OPTProb.fPeaks,OPTProb.holes] = zCreation(impedNFO);
	
	%%     Creating  a first interesting design space prediction space with initsols
	% if the candidate is the first one to test (the one in x.txt
	for j=OPTProb.reg
		if OPTProb.Workers>1
			SimuPerWorker=OPTProb.PmZone/OPTProb.Workers;
			NotesSep=cell(OPTProb.Workers,1);AllNotesSep=cell(OPTProb.Workers,1);PowSep=cell(OPTProb.Workers,1);
			parfor k=1:OPTProb.Workers
				OPTProbWorker=OPTProb
				OPTProbWorker.PmZone=SimuPerWorker;
				[NotesSep{k},AllNotesSep{k},PowSep{k}]=SimuHB(OPTProbWorker,Candidate,j);
			end
			Notes{i,j}=cat(1,NotesSep{:});
			AllNotes{i,j}=cat(1,AllNotesSep{:});
			Pow{i,j}=cat(1,PowSep{:});
		else
			[Notes{i,j},AllNotes{i,j},Pow{i,j}]=SimuHB(OPTProb,Candidate,j);
		end
	end
	if OPTProb.plot
		PLOTSimuOptim(Notes(i,:),INSTRUS{i},Candidate);%just to see some stuff
	end
	
end
if OPTProb.Workers>1
	spmd
		% 		pwd
		cd('../..');
		rmdir(['tmp/Worker' num2str(w.ProcessId)],'s');
	end
end
end
