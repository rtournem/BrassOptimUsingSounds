 function [BBO,Uncertainty,info]=CritCalc(OPTProb,Notes)
Criteria=OPTProb.desc(:,1);
BBO=zeros(1,length(Criteria));
Uncertainty=zeros(1,length(Criteria));
%checking Notes validity:
for fing=OPTProb.fing
	for reg=OPTProb.reg
		if isempty(Notes{fing,reg}) || size(Notes{fing,reg},1)<OPTProb.PmZone*...
				size(unique(reshape([OPTProb.desc{:,2}], 2, [])', 'rows'), 1)
			%if one regime didn't converge enough then we give up
			error('Cannot compute Criterion because data uncomplete');
		end
	end
end
for k=1:length(Criteria)
	switch Criteria{k}
		case 'inharmpm'
			%selecting the notes that will be used for the criterion calculation
			for fing=OPTProb.fing
				for reg=OPTProb.reg
					%funny way to gather the content of the next column knowing the line
					Pms=OPTProb.desc{circshift(strcmp(OPTProb.desc,'inharmpm'),1,2)};
					FplayTaken{fing,reg} = Notes{fing,reg}(Pms(1)<Notes{fing,reg}(:,1) &...
						Pms(2)>Notes{fing,reg}(:,1),4);
				end
			end
			%criterion calculation
			[Val,Uncertainty(k),info{k}]=InharmCalc(OPTProb,FplayTaken); %works only with one fingering currently
		case 'cgspm'
			clear cgsNotes
			cgsNotes=[];
			%selecting the notes that will be used for the criterion calculation
			for fing=OPTProb.fing
				for reg=OPTProb.reg
					Pms=OPTProb.desc{circshift(strcmp(OPTProb.desc,'cgspm'),1,2)};%funny way to gather the content of the next column knowing the line
					NotesTaken = Notes{fing, reg}(Pms(1) < Notes{fing, reg}(:, 1) &...
						Pms(2) > Notes{fing, reg}(:, 1), :);
					%CGS calculation
					N=(size(Notes{fing,reg},2)-6)/2;
					CGSREG=abs(NotesTaken(:,5:5+N)+1i*NotesTaken(:,6+N:6+2*N))*(0:N)'./sum(abs(NotesTaken(:,5:5+N)+1i*NotesTaken(:,6+N:6+2*N)),2);
					cgsNotes=[cgsNotes;CGSREG repmat(reg,size(CGSREG,1),1)];%le cgs de chaque note!
					cgsMeanReg(reg)=mean(CGSREG);
					UncertaintyReg(reg)=std(CGSREG)/sqrt(size(CGSREG,1));
				end
			end
			%I want to compute the mean of the CGS of each note but the mean of the
			%mean is the mean if the effectiv are of the same size.
			%Consequently, I just have to compute the mean of the CGS of every note.
			Val=-mean(cgsNotes(:,1)); 
			Uncertainty(k)=1.96*sqrt(sum(UncertaintyReg.^2));%formula of propgation d'incertitude
			UncertaintyReg=1.96.*UncertaintyReg;
			info{k}={cgsMeanReg,UncertaintyReg};
		case {'inharmplasticity','cgsplasticity'}
			for problems={'inharmplasticity','cgsplasticity'} %WARNING DON'T DO BOTH AT THE SAME TIME (or modify the code)
				AllPms=[OPTProb.desc{circshift(strcmp(OPTProb.desc,problems),1,2)}];%funny way to gather the content of the next column knowing the line
			end
			clear cgsNotes
			cgsNotes{1}=[];cgsNotes{2}=[];
			FingRegMap= false(max(OPTProb.fing),max(OPTProb.reg));
			%selecting the notes that will be used for the criterion calculation
			for fing=OPTProb.fing
				for reg=OPTProb.reg
					for IdxPms=1:size(AllPms,1) %pour 2 range Pm extr???me
						Pms=AllPms(IdxPms,:);
						NotesTaken = Notes{fing, reg}(Pms(1) < Notes{fing, reg}(:, 1) &...
							Pms(2) > Notes{fing, reg}(:, 1), :);
						disp(length(NotesTaken));
						fbar{IdxPms}(fing,reg)=mean(NotesTaken(:,4));%line for inharmplasticity
						N=(size(Notes{fing,reg},2)-6)/2;%next 3 lines for cgsplasticity
						CGSREG{IdxPms}=abs(NotesTaken(:,5:5+N)+1i*NotesTaken(:,6+N:6+2*N))*(0:N)'./sum(abs(NotesTaken(:,5:5+N)+1i*NotesTaken(:,6+N:6+2*N)),2);
						cgsNotes{IdxPms}=[cgsNotes{IdxPms};CGSREG{IdxPms} repmat(reg,size(CGSREG{IdxPms},1),1)];%le cgs de chaque note!
					end
					%for inharmplasticity
					FingRegMap(fing,reg)=true;%map of the used note
					fbardiff(fing,reg)=1200*log2(fbar{2}(fing,reg)./fbar{1}(fing,reg));%line for inharmplasticity
					%for cgsplasticity
					cgsMeanReg(reg)=mean(CGSREG{1})-mean(CGSREG{2});
					UncertaintyReg(reg)=(std(CGSREG{2})/sqrt(size(CGSREG{2},1)))^2+(std(CGSREG{1})/sqrt(size(CGSREG{1},1)))^2;
				end
			end
			if strcmp(Criteria{k},'inharmplasticity')
				Val(k)=mean(abs(fbardiff(FingRegMap)));
			else
				Val(k)=mean(cgsMeanReg);
				Uncertainty(k)=1.96*sqrt(sum(UncertaintyReg));
				UncertaintyReg=1.96*sqrt(UncertaintyReg);
				info{k}={cgsMeanReg,UncertaintyReg};
			end
	end
	if ~isempty(OPTProb.desc{k,4}) %if it is a constraint
		%sign(H)*(|H|-BBO) positive is bad.
		BBO(k)=sign(OPTProb.desc{k,4})*(abs(OPTProb.desc{k,4})-Val); 
		disp(['Val is : ' num2str(Val) ', which makes a constraint value of : '...
			num2str(num2str(BBO(k))) ' (positive means the constraint is not fullfilled)']);
	else
		BBO(k)=Val;
		disp(num2str(BBO(k)));
	end
end
 end
