function Pis=InitVectorOPTIM(OPTProb,Mask,Candidate,AllNotes,fPeaks,holes,strat)
%Provides the simulation with initial vectors for the harmonic balance technique.

SystematicTry=OPTProb.SystematicTry;
reg=OPTProb.CurReg;
N=OPTProb.Simu.N;
precision=2;
muaInter=0.5;
faInter=3;
AllOlderSol=0;
Pis=[];
if SystematicTry==2 %then we want every possibility
	OPTProb.SystematicTry=0;
	Pis1=InitVectorOPTIM(OPTProb,Mask,Candidate,AllNotes,fPeaks,holes,strat);
	OPTProb.SystematicTry=1;
	Pis2=InitVectorOPTIM(OPTProb,Mask,Candidate,AllNotes,fPeaks,holes,strat);
	Pis=[Pis1;Pis2];
	return;
end
if SystematicTry
	idxendReg=find(holes>fPeaks(reg),1,'first');
	if ~isempty(cell2mat(strfind(strat,'largeBW')))
		WatchedRegimes=[holes(idxendReg-1)' holes(idxendReg)' ];
	elseif ~isempty(cell2mat(strfind(strat,'lowBW')))
		WatchedRegimes=[holes(idxendReg-1)' fPeaks(reg)'+10];
	elseif ~isempty(cell2mat(strfind(strat,'highBW')))
		%-10 for the notes slightly under the peak (shouldn't happen for a trumpet)
		WatchedRegimes=[fPeaks(reg)-10 holes(idxendReg) ];
		%shortening the vector because we should be that wrong
		WatchedRegimes=[fPeaks(reg)-10 (holes(idxendReg)+fPeaks(reg))/2 ];
	end
	Finits=WatchedRegimes(1):precision:WatchedRegimes(2);
	Pis=partialCreation(Mask,Finits,N,strat);
else
	%random successful try, because we are lucky
	if ~isempty(AllNotes)
		if AllOlderSol
			Pis=AllNotes(:,5:end);
			Pis(:,N+3)=AllNotes(:,4);
		else
			Pis=AllNotes(end-min(size(AllNotes,1)-1,4):end,5:end);
			Pis(:,N+3)=AllNotes(end-min(size(AllNotes,1)-1,4):end,4);
		end
	end
	%Number of Neighbor layer you want.
	NeighborStrat=strfind(strat,'AllNeighbors_Depth');
	idxInfoNeigh = not(cellfun('isempty', NeighborStrat));
	depth=str2double(strat{idxInfoNeigh}(end));	%you have to abs all the next line because mua is negative.
	
	VarMax=max(abs(OPTProb.INITSOLS));
	VarMin=min(abs(OPTProb.INITSOLS));
	slope=VarMax-VarMin;
	
	DistMus=abs(OPTProb.INITSOLS(:,1:3)-repmat(Mask,size(OPTProb.INITSOLS,1),1));
	%InitSols is not like all note because after the solution it has the
	%regime number and the candidate variables.
	DistCand=abs(OPTProb.INITSOLS(:,8+2*N:end)-repmat(Candidate,size(OPTProb.INITSOLS,1),1));
	%normalizing the distances
	for i=1:3
		DistMus(:,i)=DistMus(:,i)./slope(:,i);
	end
	for i=1:length(Candidate)
		DistCand(:,i)=DistCand(:,i)./slope(:,7+i+2*N);
	end
	Dist=[DistMus ,DistCand];
	Dist=sqrt(sum(Dist.^2,2));
	[~,idx]=sort(Dist);
% 	disp('------------ closest Distance ----------');
% 	disp(Dist(idx(1:10)));
% 	disp('----------------------------------------');
	%taking the five first neighbor
	NoteNeighbor=OPTProb.INITSOLS(idx(1:min(size(OPTProb.INITSOLS,1),depth*5)),:);
	
	if ~isempty(NoteNeighbor)
		%interpolated result
		WeightNeigh=1./Dist(idx(1:min(size(OPTProb.INITSOLS,1),depth*5)));
		InterpNote=(WeightNeigh'*NoteNeighbor)./sum(WeightNeigh);
		
		if exist('Soundmodeled','var')
			%WITH MODEL+INTERP+NEIGHBORS
			Pis=[Soundmodeled(2:3+2*N);InterpNote(:,5:6+2*N);NoteNeighbor(:,5:6+2*N);Pis];
			Pis(1,N+3)=Soundmodeled(1);
			Pis(2,N+3)=InterpNote(:,4);
			Pis(3:size(NoteNeighbor,1)+2,N+3)=NoteNeighbor(:,4);
		else
			%WITHOUT MODEL ONLY INTERP+NEIGHBORS
			Pis=[InterpNote(:,5:6+2*N);NoteNeighbor(:,5:6+2*N);Pis];
			Pis(1,N+3)=InterpNote(:,4);
			Pis(2:size(NoteNeighbor,1)+1,N+3)=NoteNeighbor(:,4);
		end
		
	end
end
end
