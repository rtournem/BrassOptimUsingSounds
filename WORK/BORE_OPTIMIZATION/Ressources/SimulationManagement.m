function [Pms, Marker, OPTProb]=SimulationManagement(OPTProb, SelectedNotes,...
	PowNotes, Candidate)
%this is the chief function managing the optimization, it is able to known for
%every dynamic range if enough simulations have been computed and decide on
%whether we should compute other simulation or not.

DescFormatted=OPTProb.desc';
ObjCstString=[DescFormatted{1,:}]; %string of every optimization problem name in order to make strfind works easily

if strfindMutiPatterns(ObjCstString,{'inharmplasticity','cgsplasticity'})%we first treat the plasticity part since it will, by definition, sweep all the possible pm values.
	%%%%%%%%%%% finding all the Pms intervals %%%%%%%%%%%%%%
	for problems={'inharmplasticity','cgsplasticity'}
		AllPms=[OPTProb.desc{circshift(strcmp(OPTProb.desc,problems),1,2)}];%funny way to gather the content of the next column knowing the line
		if ~isempty(AllPms)
			break;
		end
	end
	%%%%%%%%%%% start of the simulation process lets make a bunch of mask in the first Pm interval %%%%%%%%%%%%%
	if isempty(SelectedNotes)
		Pms=AllPms(1,:); %we have to start with the first interval
		[OPTProb, Marker] = PointSelection(Candidate, OPTProb, Pms);
		return;
		%%%%%%%%%%% the simulations already started: should we fill a zone or start a new one %%%%%%%%%%%%%%%%%
	else
		for Pmidx=1:size(AllPms, 1) %we are looking for the current pm interval to fill
			idxNotes = SelectedNotes(:, 1) >= AllPms(Pmidx, 1) & SelectedNotes(:, 1) <=...
				AllPms(Pmidx, 2) & PowNotes > OPTProb.THRESHOLDSCurIdxPM(OPTProb.CurReg);
			%%%%%%%%%%%% if we are in a new pm interval, a new bunch has to be filled %%%%%%%%%
			%dispstat(['notes prises ' num2str(sum(idxNotes))], 'keepthis');
			if ~idxNotes
				Pms=AllPms(Pmidx,:);
				[OPTProb, Marker] = PointSelection(Candidate, OPTProb, Pms);
				return;
				
				%%%%%%%%%%%% we just have to finish to fill the zone %%%%%%%%%%%
			else
				if size(unique(SelectedNotes(idxNotes, 1 : 3), 'rows'), 1) < OPTProb.PmZone
					Pms=AllPms(Pmidx,:); %we have to start with the first interval
					[OPTProb, Marker] = PointSelection(Candidate, OPTProb, Pms);
					return;
				end
			end
		end
	end
end

%IT IS GOING TO BE THE SAME CODE WITH TINY FUNDAMENTAL DIFFERENCES:
%-WE RUN PROBLEMS ONE AFTER THE OTHER SINCE WE CAN HAVE DIFFERENT PM INTERVAL FOR 2 DIFFERENT DESCRIPTORS (inharmpm pm=[4000 5000], cgspm pm=[8000 11000])
%-WE HAVE ONLY ONE PM INTERVAL
%-WE CHECK THE SOL IN SELECTED NOTES THAT ARE INSIDE THE PM INTERVAL AND ABOVE THE THRESHOLD

for Problem = {'cgspm', 'inharmpm', 'cgshomo'}
	if strfind(ObjCstString, char(Problem)) %si on a un de ces descripteurs dans nos desiderat
		%%%%%%%%%%% finding the Pm interval %%%%%%%%%%%%%%
		%There isn't several sets of dynamics because of the descriptor studied.
		Pms = OPTProb.desc{circshift(strcmp(OPTProb.desc,Problem),1,2)};%funny way to gather the content of the next column knowing the line
		%%%%%%%%%%% start of the simulation process lets make a bunch of mask in the first Pm interval %%%%%%%%%%%%%
		[OPTProb, Marker] = PointSelection(Candidate, OPTProb, Pms);
		if ~isempty(SelectedNotes)
			idxNotes = SelectedNotes(:, 1) >= Pms(1) & SelectedNotes(:, 1) <= Pms(2) &...
				PowNotes > OPTProb.THRESHOLDSCurIdxPM(OPTProb.CurReg);
			%%%%%%%%%%%% if we are in a new pm interval, a new bunch has to be filled %%%%%%%%%
			if isempty(idxNotes)
				return;
				%%%%%%%%%%%% we just have to finish to fill the zone %%%%%%%%%%%
			else
				if size(unique(SelectedNotes(idxNotes, 1 : 3), 'rows'), 1) < OPTProb.PmZone
					return;
				else
					%%%%%%%%%% lets see the next interval %%%%%%%%%%%%
					Pms=0;
					continue;
				end
			end
		end
	end
end
%%%%%%%%%%% if every simuation finished lets move ON %%%%%%%%%%%%%%%%%%%%%%
if ~exist('Pms','var')
	Pms=0;Marker=0;
end
end
function [OPTProb, Marker] = PointSelection(Candidate, OPTProb, Pms)

idxPms = cellfun(@(x) isequal(x, Pms), OPTProb.PMS, 'UniformOutput', 1);
Marker=find(idxPms)-1;
OPTProb.THRESHOLDSCurIdxPM = OPTProb.THRESHOLDS{idxPms};
OPTProb.neighborsEntireIdxPM = OPTProb.neighborsEntire{idxPms};

idxReg = OPTProb.MARKERS == 10 * Marker + OPTProb.CurReg;
OPTProb.CloudReg = OPTProb.INITSOLS(idxReg, [1 : 3, end - length(Candidate) + 1 : end]);
OPTProb.minValReg = repmat(OPTProb.minVal{idxPms,OPTProb.CurReg}, size(OPTProb.CloudReg, 1), 1);
OPTProb.maxValReg = repmat(OPTProb.maxVal{idxPms,OPTProb.CurReg}, size(OPTProb.CloudReg, 1), 1);
end
