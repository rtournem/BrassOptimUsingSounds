function PLOTSimuOptim(Notes,Instru,Candidate)
global N_SIMU
N=N_SIMU;
AllNotes=[];
for i=1:length(Notes(:))
	AllNotes=[AllNotes ; Notes{i}];
end
if isempty(AllNotes)
	return;
end
[~,~,InstruNFO,~,~,PARAMimped]=GeneralParamEdition;
[~,fPeaks,holes]=ImpedDef(Instru,InstruNFO,Candidate,PARAMimped);
AllNotes(:,1:2)=zeros(size(AllNotes,1),2);
PowSol=AllNotes(:,5:N+5)+1i*AllNotes(:,N+6:end);
PowSol=sqrt(sum(abs(PowSol).^2,2));
PowSol=PowSol./AllNotes(:,1);
pmmuafa=AllNotes(:,1:3);
PLOTNotes(AllNotes,PowSol,N,pmmuafa,zeros(1000000,1),[Instru '_' num2str(Candidate)]...
	,fPeaks,holes,'useless',[1 0]);


end