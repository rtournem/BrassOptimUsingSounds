function [Val,Uncertainty,info]=InharmCalc(OPTProb,Fplay)
%for one fing only right now !!!! (simplified version of the inharm toolbox

names={'C' 'Db' 'D' 'Eb' 'E' 'F' 'Gb' 'G' 'Ab' 'A' 'Bb' 'B'};
pitch={'-1' '0' '1' '2' '3' '4' '5' '6' '7' '8' '9'};

keySet =   {1,2,3,4};
valueSet = {{'E2','Bb3','F4','Bb4','D5','F5','Ab5','Bb5'};
	{'D2','Ab3','Eb4','Ab4','C5','Eb5','Gb5','Ab5'};
	{'Eb2','A3','E4','A4','Db5','E5','G5','A5'};
	{'C2','Gb3','Db4','Gb4','Bb4','Db5','E5','Gb5'}};
FingRegNote=containers.Map(keySet,valueSet);
WellTempDist=zeros(12,12);
for j=1:11
	WellTempDist=WellTempDist+diag(100*j.*ones(1,12-j),j)+diag(100*(-j).*ones(1,12-j),-j);
end

%data formatting
for l=OPTProb.reg
	Fmean(l)=mean(Fplay{OPTProb.fing,l});
	Fsigma(l)=std(Fplay{OPTProb.fing,l});
	NFplay(l)=size(Fplay{OPTProb.fing,l},1);
end

Settings=OPTProb.desc{circshift(strcmp(OPTProb.desc,'inharmpm'),2,2)};
ref=Settings{1};

for l=OPTProb.reg
	if l~=ref
		%finding the ideal distance in cents in the well-tempered scale.
		Notes=FingRegNote(OPTProb.fing);
		
		NoteRef=Notes{ref};
		OctRef=str2double(NoteRef(end));
		NoteRefIdx= strcmp(NoteRef(1:end-1),names);
		NoteCur=Notes{l};
		OctCur=str2double(NoteCur(end));
		NoteCurIdx= strcmp(NoteCur(1:end-1),names);
		
		Ideal=1200*(OctCur-OctRef)+WellTempDist(NoteRefIdx,NoteCurIdx);
		
		ETD(l)=1200*log2(Fmean(l)/Fmean(ref))-Ideal;
		estReg(l)=abs(ETD(l));
		
		%calcul sans propgation de l'incertitude me semble t'il  PAS BON
% 		UncertaintySupReg(l)=1200*log2((Fmean(l)+1.96*Fsigma(l)/sqrt(NFplay(l)))/(Fmean(ref)-1.96*Fsigma(ref)/sqrt(NFplay(ref))))-Ideal;
% 		UncertaintyInfReg(l)=1200*log2((Fmean(l)-1.96*Fsigma(l)/sqrt(NFplay(l)))/(Fmean(ref)+1.96*Fsigma(ref)/sqrt(NFplay(ref))))-Ideal;
% 		UncertaintyReg(l)=abs(ETD(l)-UncertaintySupReg(l));
		
		%meilleur calcul avec propagation de l'incertitude relative PAS BON
		RatioRelativUncertainty(l)=(Fsigma(l)/sqrt(NFplay(l)))/Fmean(l)+(Fsigma(ref)/sqrt(NFplay(ref)))/Fmean(ref);
		UncertaintySupReg(l)=1200*log2((Fmean(l)/Fmean(ref))*(1+1.96*RatioRelativUncertainty(l)))-Ideal;
		UncertaintyReg(l)=abs(ETD(l)-UncertaintySupReg(l));
		
		%propagation d'incertitude g�n�ral BON (Ref. GUM 1995, formule 10)
		UncertaintyReg(l)=(1200/((length(OPTProb.reg)-1)*log(2)))^2*((Fsigma(l)/sqrt(NFplay(l))/Fmean(l))^2+...
			((Fsigma(ref)/sqrt(NFplay(ref)))/Fmean(ref))^2);
	end
end
% disp('FAUX: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
% disp(ETD-UncertaintyInfReg);
% disp('VRAI: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%');
% disp(UncertaintySupReg-ETD);
% disp(1.96*sqrt(UncertaintyReg));
Val=sum(estReg)/(length(OPTProb.reg)-1);
%calcul logique propagation d'incertitude
% Uncertainty=sum(UncertaintyReg);
%calcul rep�r� dans acoustique et technique???
% Uncertainty=sqrt(sum(UncertaintyReg.^2));
Uncertainty=1.96*sqrt(sum(UncertaintyReg));
info={estReg,UncertaintyReg};
end