function [SelectedNotes,ForceSkip,STOP,AllNotes,PowNotes]=NoteValidation(OPTProb,Mask,Pfinal,SelectedNotes,holes,ForceSkip,AllNotes,PowNotes)
%AllNotes gather all new solutions powerfull or not, wether SelectedNote
%keep the best solution.
STOP=0;
N=OPTProb.Simu.N;
%if the found note is played in the right regime
if Pfinal(N+3)>holes(OPTProb.CurReg) && Pfinal(N+3)<holes(OPTProb.CurReg+1)+20
	NewNote=[Mask, Pfinal(N+3), Pfinal];
	NewNote(N+7)=0;
	AllNotes=AddIfNew(N,AllNotes,Mask,Pfinal);
	%is the note efficient enough?
	PowSol=NewNote(5:N+5)+1i*NewNote(N+6:end);
	PowSol=sqrt(sum(abs(PowSol).^2));
	PowSol=PowSol/NewNote(1);
	if  OPTProb.fastConv
		if PowSol<OPTProb.THRESHOLDSCurIdxPM(OPTProb.CurReg)
			ForceSkip=ForceSkip+0.2;
			if floor(ForceSkip)
% 				disp('too bad... next please!');
				%if this mask produce 10 solutions with low energy, we skip it
				ForceSkip=0;
				STOP=1;
			end
		else
			SelectedNotes=[SelectedNotes; NewNote];
			PowNotes=[PowNotes; PowSol];
			STOP=1;
		end
	else
		if PowSol>OPTProb.THRESHOLDSCurIdxPM(OPTProb.CurReg)
			SelectedNotes=AddIfNew(N,SelectedNotes,Mask,Pfinal,PowSol);
		end
	end
else
	% 		disp(['Point converging toward wrong regime: fplay=' num2str(Pfinal(N+3))]);
end
end
function AllNotes=AddIfNew(N,AllNotes,Mask,Pfinal,Pow)
%s'assurer de ne pas mettre 2 fois le m�me vecteur:

%initialisation: si le vecteur AllNotes est vide
if isempty(AllNotes)
	AllNotes=[AllNotes; Mask, Pfinal(N+3), Pfinal];
	AllNotes(end,N+7)=0;
else
	%si le musicien n'a pas encore converg�
	if isempty(find(ismember(AllNotes(:,1:3),Mask,'rows'), 1))
		AllNotes=[AllNotes; Mask, Pfinal(N+3), Pfinal];
		AllNotes(end,N+7)=0;
	else
		if exist('Pow','var')
			idxOldSol=find(ismember(AllNotes(:,1:3),Mask,'rows'), 1);
			PowOldSol=AllNotes(idxOldSol,5:N+5)+1i*AllNotes(idxOldSol,N+6:end);
			PowOldSol=sqrt(sum(abs(PowOldSol).^2));
			PowOldSol=PowOldSol/AllNotes(idxOldSol,1);
			if PowOldSol<Pow
				%we replace the selected sol by the new one if better.
				AllNotes(idxOldSol,:)=[Mask, Pfinal(N+3), Pfinal];
			end
		else
			%s'il a converg� mais pas encore vers cette note
			if isempty(find(abs(AllNotes(ismember(AllNotes(:,1:3),Mask,'rows'),4)-Pfinal(N+3))<10^-3, 1))
				AllNotes=[AllNotes; Mask, Pfinal(N+3), Pfinal];
				AllNotes(end,N+7)=0;
			end
		end
	end
end
%we stop at bisolutions Comment if not needed
% 	MusiFoundFreqs=AllNotes(ismember(AllNotes(:,1:3),Mask,'rows'),4);
% 	TargetReg=find(holes-min(MusiFoundFreqs)>0,1,'first')-1;
% 	if ~isempty(TargetReg)
% 		if length(find(fPeaks(TargetReg)-10<MusiFoundFreqs & MusiFoundFreqs<holes(TargetReg+1)))==2
% 			STOP=1;
% 		end
% 	end
end
