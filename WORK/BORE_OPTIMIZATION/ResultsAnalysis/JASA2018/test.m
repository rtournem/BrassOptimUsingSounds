	figure;
	Delaunayplotting(Candidates,{discBBO}); %if too much under ->  problem
	hold on
	if server == 3 || server == 4
		trisurf(data{1}.crossingTri, data{1}.x1LinClean, data{1}.x2LinClean,...
			dataBBO{1}.toPlotLinClean, 'EdgeColor', 'none', 'FaceColor', 'g');
	end
	plot3(bestcandidate(:, 1), bestcandidate(:, 2), bBBOs(:, end),  '.b',...
		'markersize', 20);
	
	figure;
	Delaunayplotting(Candidates,{discBBO}); %if too much under ->  problem
	hold on
	if server == 1 || server == 2 || server == 5 ||  server == 6
		plot3(candidatesProf(:), candidatesRad(:), BBOs(:), '.r', 'markersize', 10);
	elseif server == 3 || server == 4
		trisurf(data{1}.crossingTri, data{1}.x1LinClean, data{1}.x2LinClean,...
			dataBBO{1}.toPlotLinClean, 'EdgeColor', 'none', 'FaceColor', 'g');
		plot3(candidatesProf(const < 0), candidatesRad(const < 0), BBOs(const < 0),...
			'.r', 'markersize', 10);
		plot3(candidatesProf(const > 0), candidatesRad(const > 0), BBOs(const > 0),...
			'.k', 'markersize', 10);
	end
	plot3(candidatesProf(:, 1), candidatesRad(:, 1), BBOs(:, 1), 'xk',...
		'markersize', 20, 'linewidth', 5);
	plot3(bestcandidate(:, 1), bestcandidate(:, 2), bBBOs(:, end), '.b',...
		'markersize', 40);
