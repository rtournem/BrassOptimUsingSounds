function [fig, FontSize, FontSizeTicks, namefig, figBW] = histplot(fig2plot, FontName)
global cLo cEn
switch fig2plot
	case 7
		res1 = '1';
		res2 = '2';
		ylab = 'Objective function (cents)';
		namefig = 'histplot2Dintonation';
		locleg = 'northeast';
		FontSize = 45;
		FontSizeTicks = 30;		
	case 9
		res1 = '3';
		res2 = '4';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot2DSCaverage';
		locleg = [];
		FontSize = 45;
		FontSizeTicks = 30;
	case 19
		res1 = '5';
		res2 = '6';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot2DSCdynamics';
		locleg = 'east';
		FontSize = 45;
		FontSizeTicks = 30;
	case 29
		res1 = '3';
		res2 = '4';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot2DSCaverageLine';
		locleg = [];
		FontSize = 54;
		FontSizeTicks = 40;
		pos = [200, 200, 800, 800];
		
	case 39
		res1 = '5';
		res2 = '6';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot2DSCdynamicsLine';
		locleg = 'east';
		FontSize = 54;
		FontSizeTicks = 40;
		pos = [200, 200, 800, 800];
	case 10
		res1 = '7';
		res2 = '8';
		ylab = 'Objective function (cents)';
		namefig = 'histplot10Dintonation';
		locleg = 'northeast';
		FontSize = 60;
		FontSizeTicks = 45;
	case 20
		res1 = '9';
		res2 = '10';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot10DSCaverage';
		locleg = [];
		FontSize = 60;
		FontSizeTicks = 45;
	case 30
		res1 = '11';
		res2 = '12';
		ylab = 'Objective function (SC value)';
		namefig = 'histplot10DSCdynamics';
		locleg = [];
		FontSize = 60;
		FontSizeTicks = 45;
end
nbMarker = 10;
LineW = 2;
MkS = 15;
load(['result_' res1]);
bboEn = bBBOs;
load(['result_' res2]);
bboLo = bBBOs;
if ~exist('pos', 'var')
	pos = [200, 200, 1100, 700];
end

for i = 1:2
figStyl(i) = figure(	'position', pos);
%history plot de la moyenne pour chaque step de toutes les optims
if i == 1
	cE = cEn;
	cL = cLo;
else
	cE = c2g(cEn);
	cL = c2g(cLo);
end
pointeur(1) = plot(1 : evalNumber,mean(bboEn(:, 1 : evalNumber)));
legendText{1} = 'Ensemble';

hold on
pointeur(2) = plot(1 : evalNumber,mean(bboLo(:, 1 : evalNumber)));
legendText{2} = 'LOWESS';
if ~isempty(locleg)
	legend(pointeur,legendText, 'FontSize', FontSize, 'location',locleg, 'FontName', FontName);
end
if fig2plot ~=30 && fig2plot ~=39
	ylabel(ylab);
end
xlabel('Number of blackbox evaluations');
end
fig = figStyl(1);
figBW = figStyl(2);
end
