clear all
% close all
global ROOT
ROOT = '../../../../';
path(path, genpath([ROOT 'REDUNDANT']));
path(path, genpath('/Ressources'));
DataPath = [ROOT 'DATA/optim/JASA2018/'];
path(path,genpath([ROOT 'WORK/BORE_OPTIMISATION/BBoxDisc/Ressources']));

%% parameters: which servers and which repe do you want to analyze:
% server1-2: 2D justeese (OECV-LOWESS) server3-4: 2D maxCGS under 12cent max
% server5-6: 2D max cgsplasticity server7-12 same thing for 10D
% server23: server3 avec mi au lieu de max server25 min au lieu de max

%toutes les r�p�s pr�cis�ment
% % % repesAll ={1 : 20 ;%1
% % % 	1 : 19; %2
% % % 	1 : 10; %3
% % % 	1 : 14; %4
% % % 	1 : 14; %5
% % % 	1 : 10; %6
% % % 	1 : 10; %7
% % % 	0;      %8
% % % 	1 : 10; %9
% % % 	0;      %10
% % % 	0;      %11
% % % 	0;      %12
% % % 	0;0;0;0;0;0;0;0;0;0; %13 - 22
% % % 	1 : 16; %23
% % % 	1 : 7;  %24
% % % 	1 : 2;  %25
% % % 	0;      %26
% % % 	1 : 9;  %27
% % % 	};

%les r�p�s qui nous int�ressent
repesAll(1 : 27) = {1 : 10};

%1 if you want to check every repetition, 0 if you want the global results
%directly
repeplot = 1;
server = 1;
repes = repesAll{server};

%fontsize:
ftsz = 14;
%taille figure
posfig =  [270, 0, 900, 900];
%% Processing

%design space parameters
if server < 7 || server > 20
	dim = 2;
	intervalles = [0 0.0025; 0.0014 0.0022];
	evalNumber = 100;
else
	dim = 10;
	intervalles = repmat([0.0045 0.006], 10, 1);
	evalNumber = 200;
end

%loading the discretisation for the 2D case
if server < 7 || server > 20 %for 2D case:
	Candidates={0 : 0.00012 : 0.0025, 0.0014 : 0.00004 : 0.0022}; %tr�s fin
	Problem.catalyst = 'MP2TMM_3idxPM';
	Stamp = '_2018_4_11_16_16_49';
	Problem.name = 'MP2TMM';
	dataPath = [ROOT 'DATA/optim/discretisation/' Problem.catalyst '/' Stamp];
	if server == 1 || server == 2
		load([dataPath '/results/result_AVECCRIT' Problem.name]);
	elseif server == 3 || server == 4
		load([dataPath '/results/result_AVECCRIT' Problem.name]);
		discCons = BBO; %pour afficher la limite
		load([dataPath '/results/resultCGS_' Problem.name]);
	elseif server == 25 || server == 26 || server == 5 || server == 6
		Stamp = '_2018_5_28_9_48_11';
		dataPath = [ROOT 'DATA/optim/discretisation/' Problem.catalyst '/' Stamp];
		load([dataPath '/results/result_CGSPlasticity' Problem.name]);
	end
	discBBO = BBO;
end


bestcandidate = zeros(length(repes), dim);
BBOs = zeros(length(repes), 100);
const = zeros(length(repes), 100);
bBBOs = zeros(length(repes),100);
candidatesProf = zeros(length(repes),100);
candidatesRad = zeros(length(repes),100);
idx = 0;
for repe = repes
	idx = idx + 1;
	legendName{idx} = ['repe ' num2str(repe)];
	
	%organizing the folder containing all the results (pretty tricky but general
	%way to treat this problem)
	files = dir([DataPath 'server' num2str(server) '/repe' num2str(repe) '/Results']);
	names = {files.name};
	ResultsName = cellfun(@(x) x(1:end-4), names(cellfun(@(x) ~isempty(strfind(x,...
		'2018')), names)), 'Uniformoutput', false);
	%la difficult� de la ligne qui suit provient du fait que lors de l'�criture du
	%fichier matlab transforme les miliseconde au passage en string (supprime les 0
	%inutiles en fait)
	dateOfResults = cellfun(@(x) [x(1:4) '-' x(5:6) '-' x(7:8) ' ' x(9:10) ':'...
		x(11:12) ':' x(13:14) '.' x(15:end)],	ResultsName, 'Uniformoutput', false);
	
	dateOfResults = datetime(dateOfResults, 'Format', 'yyyy-MM-dd HH:mm:ss.SSS');
	
	[~, ResultIdx] = sort(dateOfResults);
	
	%checking we have the 100 repetitions
	if length(ResultsName) < evalNumber
		warning(['Attention! Il n''y a que ' num2str(length(ResultsName))...
			' points � traiter pour la r�p�tition ' num2str(repe)]);
	else
		disp(['nombre de points parfait!!! repe n�' num2str(repe)]);
	end
	
	for i = 1 :  length(ResultsName)
% for i = 1 : 200
		try
			%loading the result file
			load([DataPath 'server' num2str(server) '/repe' num2str(repe) '/Results/'...
				ResultsName{ResultIdx(i)}]);
			candidatesProf(idx, i) = Candidate(1);
			candidatesRad(idx, i) = Candidate(2);
		catch %l'optim a fini avant la fin
			BBOs(idx, i) = BBOs(idx, i - 1);
			bBBOs(idx, i) = bBBOs(idx, i - 1);
		end
		%managing the results according to the desired objective function (min or max) and
		%constraint (server-wise)
		if server == 1 || server == 2 || server == 7 || server == 8
			BBOs(idx, i) = BBO(1);
		elseif server == 5 || server == 6 || server == 11 || server == 12
			BBOs(idx, i) = - BBO(1);
		elseif server == 3 || server == 4 || server == 9 || server == 10
			BBOs(idx, i) = - BBO(1);
			if isnan(BBO(1))
				const(idx, i) = NaN;
			else
				const(idx, i) = BBO(2);
			end
			%for display purposes:
			const(idx, 1) = NaN;
			const(idx, 100) = NaN;
		elseif server == 25 || server == 26
			BBOs(idx, i) = - BBO(1);
		end
		
		%finding the best candidate and objective value along the optimisation.
		%Les recherches de minimum
		if server == 1 || server == 2 || server == 7 || server == 8
			if i == 1
				if repe == 1
					OPTProb
				end
				if server == 7 || server == 8
					[bIntonation(idx, 1), Z10] = calcImped10D(Candidate);
				end
				bBBOs(idx ,1) = BBO(1);
			elseif BBO(1) < bBBOs(idx, i - 1)
				if server == 7 || server == 8
					disp(['Calc imped ' num2str(i)]);
					[bIntonation(idx, i), Z10] = calcImped10D(Candidate);
				end
				bestcandidate(idx, :) = Candidate;
				bBBOs(idx, i) = BBO(1);
			else
				if server == 7 || server == 8
					bIntonation(idx, i) = bIntonation(idx, i - 1);
				end
				bBBOs(idx, i) = bBBOs(idx, i - 1);
			end
			%les recherches de max
		elseif server == 3 || server == 4 || server == 25 || server == 26 ||...
				server == 9 || server == 10 || server == 5 || server == 6 ||...
				server == 11 || server == 12
			if i == 1
				if repe == 1
					OPTProb
				end
				bBBOs(idx ,1) = - BBO(1);
			elseif - BBO(1) > bBBOs(idx, i - 1)
				bestcandidate(idx, :) = Candidate;
				bBBOs(idx, i) = - BBO(1);
			else
				bBBOs(idx, i) = bBBOs(idx, i - 1);
			end
			
		end
	end
	
	%%%%%???????????????????????????????????????????????????????????
% % % % % 	%finding the best candidate and objective value along the optimisation.
% % % % % 	%Les recherches de minimum
% % % % % 	if server == 1 || server == 2 || server == 5 || server == 6 ||...
% % % % % 			server == 7 || server == 8
% % % % % 		if idx > 1 && bBBOs(idx, end) < bBBOs(idx - 1, end)
% % % % % 			bestOfTheBest =	idx;
% % % % % 		end
% % % % % 		%Les recherches de maximum
% % % % % 	elseif server == 3 || server == 4 || server == 25 || server == 26 ||...
% % % % % 			server == 9 || server == 10
% % % % % 		if idx > 1 && bBBOs{idxDuo}(idx, end) > bBBOs{idxDuo}(idx - 1, end)
% % % % % 			bestOfTheBest{idxDuo} =	idx;
% % % % % 		end
% % % % % 	end
	
	
	%plotting result for this precise repetition
	if repeplot
		try
			if server == 7 || server == 8
				figure('name', ['cas ' num2str(server) 'repe ' num2str(repe)],...
					'position', posfig);
				plot(Z10(:,1), abs(Z10(:,2) + 1i .* Z10(:,3)));
			end
			%	history plot with the all the evaluated points
			figure('Name', ['cas ' num2str(server) 'repe ' num2str(repe)],...
				'position', posfig);
			plot(bBBOs(idx, :), 'b');
			hold on
			plot(BBOs(idx, :), 'r.-', 'markersize', 10);
			if server == 7 || server == 8
				plot(bIntonation(idx, :), 'g');
			end
			%gestion du zoom en hauteur des history plot
			if server == 1 || server == 2 %|| server == 7 || server == 8
				ylim([0 10]);
			end
			if server == 3 || server == 4 || server == 5 || server == 6 ||...
					server == 9 || server == 10 || server == 11 || server == 12
				legend({'meilleures perces', 'toutes les perces �valu�es'},...
					'Fontsize', ftsz,'Location','southeast');
			elseif server == 1 || server == 2
				legend({'meilleures perces', 'toutes les perces �valu�es'},...
					'Fontsize', ftsz);
			elseif server == 7 || server == 8
				legend({'meilleures perces', 'toutes les perces �valu�es',...
					'valeur bas�e sur l''imp�dance seule'}, 'Fontsize', ftsz);
			end
			xlim([0, evalNumber]);
			ylabel('fonction objectif', 'Fontsize', ftsz);
			xlabel('n� de la boucle d''optimisation', 'Fontsize', ftsz);
			title(['history plot repe ' num2str(repe)], 'Fontsize', ftsz);
			savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
				'_repe_' num2str(repe) '_history_plot']);
			print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
				'_repe_' num2str(repe) '_history_plot'], '-dpng', '-r75');
			
			%displaying the results above their discretization for the 2D case.
			if server < 7  || server > 20%2D
				figure('name', ['cas ' num2str(server) 'repe ' num2str(repe)],...
					'position', posfig);
				dataBBO = Delaunayplotting(Candidates,{discBBO}); %if too much under ->  problem
				hold on
				plot3(candidatesProf(idx, 1), candidatesRad(idx, 1), BBOs(idx, 1), 'xk',...
					'markersize', 20, 'linewidth', 5);
				% pas de contraintes
				if server == 1 || server == 2 || server == 5 || server == 6 ||...
						server == 25 || server == 26
					plot3(candidatesProf(idx, 2 : end - 1), candidatesRad(idx, 2 : end - 1),...
						BBOs(idx, 2 : end - 1), '.r', 'markersize', 10);
				elseif server == 3 || server == 4 % pour g�rer l'affichage de la contrainte
					h = figure;
					data = Delaunayplotting(Candidates,{discCons}); %if too much under ->  problem
					close(h);
					trisurf(data{1}.crossingTri, data{1}.x1LinClean, data{1}.x2LinClean,...
						dataBBO{1}.toPlotLinClean, 'EdgeColor', 'none', 'FaceColor', 'g');
					plot3(candidatesProf(idx, const(idx, :) < 0), candidatesRad(idx,...
						const(idx, :) < 0), BBOs(idx, const(idx, :) < 0), '.r', 'markersize', 10);
					plot3(candidatesProf(idx, const(idx, :) > 0), candidatesRad(idx,...
						const(idx, :) > 0), BBOs(idx, const(idx, :) > 0), '.k', 'markersize', 10);
				end
				plot3(candidatesProf(idx, isnan(BBOs(idx, :))),...
					candidatesRad(idx, isnan(BBOs(idx, :))),...
					min(discBBO(discBBO > 0)) * ones(sum(isnan(BBOs(idx, :))), 1), '*g');
				plot3(bestcandidate(idx, 1), bestcandidate(idx, 2), bBBOs(idx, end), '.b',...
					'markersize', 40);
				
				if server == 1 || server == 2
					legend({'1 valeur de la fonction obj sur l''espace de conception',...
						'perce initiale', 'perces �valu�es', 'perces hors espace valide',...
						'perce optimale'}, 'Fontsize', ftsz, 'Location','southeast', 'box', 'off');
				elseif server == 3 || server == 4
					legend({'1 valeur de la fonction obj sur l''espace de conception',...
						'perce initiale', 'perces �valu�es', 'perces hors espace valide',...
						'perce optimale'}, 'Fontsize', ftsz, 'Location','northeast', 'box', 'off');
				end
				ylabel('rayon de grain', 'Fontsize', ftsz);
				xlabel('profondeur d''embouchure', 'Fontsize', ftsz);
				zlabel('fonction objectif', 'Fontsize', ftsz);
				title(['fonction objectif sur l''espace de conception, repe ' num2str(repe)], 'Fontsize', ftsz);
				savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
					'_repe_' num2str(repe) '_discretisation_et_optim']);
				print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
					'_repe_' num2str(repe) '_discretisation_et_optim'], '-dpng', '-r75');
				
			end
		catch %repe doesn't exists
			bBBOs(idx, :) = zeros(1, 100);
			BBOs(idx, :) = zeros(1, 100);
		end
%  		keyboard;
		close all
	end
end

figure('name', ['cas ' num2str(server) 'all'],... 					
	'position', posfig);
%history plot de toutes les optims:
plot(repmat(1 : evalNumber, length(repes), 1)', bBBOs(:, 1 : evalNumber)');

ylabel('fonction objectif', 'Fontsize', ftsz);
xlabel('n� de la boucle d''optimisation', 'Fontsize', ftsz);
title('history plot toute r�p�tition', 'Fontsize', ftsz);
savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'history_plot_touterepe']);
print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'history_plot_touterepe'], '-dpng', '-r75');

figure('name', ['cas ' num2str(server) 'all'],...
	'position', posfig);
%history plot de la moyenne pour chaque step de toutes les optims
plot(1 : evalNumber, mean(bBBOs(:, 1 : evalNumber)));

ylabel('fonction objectif', 'Fontsize', ftsz);
xlabel('n� de la boucle d''optimisation', 'Fontsize', ftsz);
title('history plot moyen', 'Fontsize', ftsz);
savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'history_plot_moyen']);
print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'history_plot_moyen'], '-dpng', '-r75');

figure('name', ['cas ' num2str(server) 'all'],... 				
	'position', posfig);
%les meilleurs candidats de chaque optim sur le m�me graphe
plot(repmat(0 : dim - 1, length(repes), 1)', bestcandidate', '.-', 'markersize', 20);
hold on
plot(repmat(0 : dim - 1, 2, 1), intervalles', '*', 'markersize', 10);

ylabel('valeur de la variable d''optimisation', 'Fontsize', ftsz);
xlabel('n� de la variable d''optimisation', 'Fontsize', ftsz);
title('perces optimales toute r�p�tition', 'Fontsize', ftsz);
savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'perces_optimales']);
print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
	'perces_optimales'], '-dpng', '-r75');

%figure par rapport � la discretisation pour le cas 2D
if server < 7 || server > 20 %2D
	%figure avec les meilleurs candidats
	figure('name', ['cas ' num2str(server) 'all'],... 	
		'position', posfig);
	dataBBO = Delaunayplotting(Candidates,{discBBO}); %if too much under ->  problem
	hold on
	
	if server == 3 || server == 4
		h = figure;
		data = Delaunayplotting(Candidates,{discCons}); %if too much under ->  problem
		close(h);
		trisurf(data{1}.crossingTri, data{1}.x1LinClean, data{1}.x2LinClean,...
			dataBBO{1}.toPlotLinClean, 'EdgeColor', 'none', 'FaceColor', 'g');
	end
	plot3(bestcandidate(:, 1), bestcandidate(:, 2), bBBOs(:, end), '.b',...
		'markersize', 20);
	
	ylabel('rayon de grain', 'Fontsize', ftsz);
	xlabel('profondeur d''embouchure', 'Fontsize', ftsz);
	zlabel('fonction objectif', 'Fontsize', ftsz);
	title(['fonction objectif sur l''espace de conception avec les'...
		'meilleures perces de chaque r�p�tition'], 'Fontsize', ftsz);
	savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
		'discretisation_et_meilleures_perces']);
	print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
		'discretisation_et_meilleures_perces'], '-dpng', '-r75');
	
	%figure avec tous les points!
	figure('name', ['cas ' num2str(server) 'all'],...
		'position', posfig);
	Delaunayplotting(Candidates,{discBBO}); %if too much under ->  problem
	hold on
	%pas de contraintes
	if server == 1 || server == 2 || server == 25 ||  server == 26 ||...
			server == 5 || server == 6
		plot3(candidatesProf(:), candidatesRad(:), BBOs(:), '.r', 'markersize', 10);
	elseif server == 3 || server == 4 %gestion de la contrainte
		trisurf(data{1}.crossingTri, data{1}.x1LinClean, data{1}.x2LinClean,...
			dataBBO{1}.toPlotLinClean, 'EdgeColor', 'none', 'FaceColor', 'g');
		plot3(candidatesProf(const < 0), candidatesRad(const < 0), BBOs(const < 0),...
			'.r', 'markersize', 10);
		plot3(candidatesProf(const > 0), candidatesRad(const > 0), BBOs(const > 0),...
			'.k', 'markersize', 10);
	end
	plot3(candidatesProf(:, 1), candidatesRad(:, 1), BBOs(:, 1), 'xk',...
		'markersize', 20, 'linewidth', 5);
	plot3(bestcandidate(:, 1), bestcandidate(:, 2), bBBOs(:, end), '.b',...
		'markersize', 40);
	
	ylabel('rayon de grain', 'Fontsize', ftsz);
	xlabel('profondeur d''embouchure', 'Fontsize', ftsz);
	zlabel('fonction objectif', 'Fontsize', ftsz);
	title(['fonction objectif sur l''espace de conception avec toutes'...
		'les perces �valu�es'], 'Fontsize', ftsz);
	savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
		'discretisation_et_optim']);
	print([ROOT 'REC_GRAPH/JASA2018/cas' num2str(server) '/' 'cas_' num2str(server)...
		'discretisation_et_optim'], '-dpng', '-r75');
	

end

save(['result_' num2str(server)], 'server', 'evalNumber', 'bBBOs', 'repes', 'dim', 'bestcandidate',...
	'intervalles');




