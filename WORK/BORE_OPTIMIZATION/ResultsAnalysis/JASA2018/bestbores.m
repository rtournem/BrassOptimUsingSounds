function [fig, FontSize, FontSizeTicks, namefig, figBW] = bestbores()
namefig = 'bore10D';
axialpos = ((9 : 2 : 31) + 0.3267) * 1e-2;
legendText = {'\textbf{Int}', '\textbf{SC - Int}', '\textbf{SC Dyn}'};
Colors{1} = {[0 0.9 0], [0.5 0.5 0], [1 0 1]};
Colors{2} = cellfun(@(x) c2g(x), Colors{1}, 'UniformOutput', false);
Markers = {'s','d', 'o'};
nbMarker = 10;
FontSize = 45;
MkS = 15;
LW = 2;
FontSizeTicks = 30;
pos = [200, 200, 1100, 590];
Candidate = [0.0047709091 0.0049018182 0.0050454545 0.0052272727 0.0054090909 0.0055363636 0.0056090909 0.0056818182 0.0057327273 0.0057763636];
cs = reshape(1 : 12, 2, 6)';

for i = 1:2

figStyl(i) = figure(	'position', pos);
plot(axialpos, [0.00464, Candidate, 0.005825]* 1e3, '--k', 'linewidth', 2);
hold on
idx2plot = 1;
for idx = 4 : 6
	load(['result_' num2str(cs(idx, 1))]);
	Cdts = bestcandidate;
	bbos = bBBOs(:, end);
	load(['result_' num2str(cs(idx, 2))]);
	Cdts = [Cdts; bestcandidate];
	bbos = [bbos; bBBOs(:, end)];
	
	switch idx
		case 4
			[~, idxB] = min(bbos);
			CdtsB = Cdts(idxB, : );
		case {5, 6}
			[~, idxB] = max(bbos);
			CdtsB = Cdts(idxB, : );
	end
	pointeur(idx2plot) = plot(axialpos,[0.00464, CdtsB, 0.005825] * 1e3);
	pointeur(idx2plot);
	idx2plot = idx2plot + 1;
end

xlim([0.093267, 0.313267]);
ylim ([0, 10]);
xlabel('Axial position (m)');
ylabel('Inner shape radius (mm)');
legend(pointeur,legendText, 'interpreter', 'latex', 'FontSize', FontSize, 'location','southeast');

end
fig = figStyl(1);
figBW = figStyl(2);
end
