clear all
close all
ROOT = '../../../../';
%fontsize:
ftsz = 14;
%taille figure
posfig =  [270, 0, 900, 900];
colors = {'b', 'r', 'g', 'c', 'y', 'm', 'k'}; %7 couleurs
cs = reshape(1 : 12, 2, 6)';
idx = 1;

load(['result_' num2str(cs(idx, 1))]);
Cdts = bestcandidate;
bbos = bBBOs(:, end);
%idx 5
% bbos(6) = bBBOs(6, end - 1);
% bBBOs(6, end) = bBBOs(6, end - 1);
figure('name', ['cas ' num2str(server) 'all'],...
	'position', posfig);
%history plot de la moyenne pour chaque step de toutes les optims
plot(1 : evalNumber, mean(bBBOs(:, 1 : evalNumber)), 'b');


load(['result_' num2str(cs(idx, 2))]);
Cdts = [Cdts; bestcandidate];
bbos = [bbos; bBBOs(:, end)];

hold on
plot(1 : evalNumber, mean(bBBOs(:, 1 : evalNumber)), 'g');

legend('OECV', 'LOWESS');
ylabel('fonction objectif', 'Fontsize', ftsz);
xlabel('n� de la boucle d''optimisation', 'Fontsize', ftsz);
title('history plot moyen', 'Fontsize', ftsz);
savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/idx_' num2str(idx)...
	'_history_plot_moyen']);
print([ROOT 'REC_GRAPH/JASA2018/idx_' num2str(idx)...
	'_history_plot_moyen'], '-dpng', '-r75');

if idx > 3
	
	CN = (Cdts - repmat(intervalles(:,1)',size(Cdts,1),1)) ./ repmat((intervalles(:, 2) - intervalles(:, 1))',size(Cdts,1),1 );
	for i=2:min(15,size(CN,1)) %30 class maximum if possible
		warning('off','stats:kmeans:FailedToConvergeRep');
		[~,~,sumd]=kmeans(CN,i,...
			'replicates',100,'MaxIter',100,'Display','off');
		WSS(i)=sum(sumd);
	end
	figure;
	plot(2:min(15,size(CN,1)), WSS(2:min(15,size(CN,1))), '*');
	drawnow;
	WSSInput=input('according to you, where is the elbow? (give the number of class you want) : ');
	mkdir([ROOT 'REC_GRAPH/JASA2018/kmeans_' num2str(idx) '/classnb_' num2str(WSSInput)]);
	idxClust=kmeans(CN,WSSInput,...
		'replicates',100,'MaxIter',100,'Display','off');
	val = figure('Name', 'valeurs groupe');
	hold on;
	for i=1:WSSInput
		idxCurFail=find(idxClust==i);
		figure(val)
		plot(i * ones(size(idxCurFail)), bbos(idxCurFail), '.',  'markersize', 20, 'color', colors{mod(i, 7) + 1} );
		text(i * ones(size(idxCurFail)), bbos(idxCurFail), num2str(idxCurFail));
		figure('name', ['cas ' num2str(idx) 'gr ' num2str(i)],...
			'position', posfig);
		%les meilleurs candidats de chaque optim sur le m�me graphe
		plot(repmat(0 : dim - 1, length(idxCurFail), 1)', Cdts(idxCurFail,:)', '.-', 'markersize', 20);
		hold on
		plot(repmat(0 : dim - 1, 2, 1), intervalles', '*', 'markersize', 10);
		legend(num2str(idxCurFail))
		ylabel('valeur de la variable d''optimisation', 'Fontsize', ftsz);
		xlabel('n� de la variable d''optimisation', 'Fontsize', ftsz);
		title('perces optimales toute r�p�tition', 'Fontsize', ftsz);
		savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/kmeans_' num2str(idx) '/classnb_' num2str(WSSInput) '/group_'...
			num2str(i)]);
		print(gcf, [ROOT 'REC_GRAPH/JASA2018/kmeans_' num2str(idx) '/classnb_' num2str(WSSInput) '/group_'...
			num2str(i)], '-dpng', '-r75');
	end
	figure(val)
	savefig(gcf, [ROOT 'REC_GRAPH/JASA2018/kmeans_' num2str(idx) '/classnb_' num2str(WSSInput) '/groupsVal']);
	print(gcf, [ROOT 'REC_GRAPH/JASA2018/kmeans_' num2str(idx) '/classnb_' num2str(WSSInput) '/groupsVal'],...
		'-dpng', '-r75');
end

