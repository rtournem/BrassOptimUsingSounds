function [fig, FontSize, FontSizeTicks, namefig, figBW] = surfaceplot(fig2plot, FontName)
global cLo ROOT cEn
MkSize = 35;
% alpha = 0.6;
FontSize = 60;
FontSizeTicks = 40;
pos = [200, 200, 800, 800];
Candidates={0 : 0.00012 : 0.0025, 0.0014 : 0.00004 : 0.0022}; %tr�s fin
Problem.catalyst = 'MP2TMM_3idxPM';
Stamp = '_2018_4_11_16_16_49';
Problem.name = 'MP2TMM';
dataPath = [ROOT 'DATA/optim/discretisation/' Problem.catalyst '/' Stamp];
switch fig2plot
	case 6
		load([dataPath '/results/result_AVECCRIT' Problem.name]);
		res1 = '1';
		res2 = '2';
		namefig = 'surfIntonation';
		pos = [200, 200, 800, 800];
	case 16
		load('Imped1');
		namefig = 'surfIntonationImped';
		pos = [200, 200, 840, 840];

	case 8
		load([dataPath '/results/result_AVECCRIT' Problem.name]);
		discCons = BBO; %pour afficher la limite
		load([dataPath '/results/resultCGS_' Problem.name]);
		namefig = 'surfSCAverage';
		res1 = '3';
		res2 = '4';
		pos = [200, 200, 1400, 800];

	case 18
		Stamp = '_2018_5_28_9_48_11';
		dataPath = [ROOT 'DATA/optim/discretisation/' Problem.catalyst '/' Stamp];
		load([dataPath '/results/result_CGSPlasticity' Problem.name]);
		namefig = 'surfSCDynamics';
		res1 = '5';
		res2 = '6';
		pos = [200, 200, 1300, 800];
end

if fig2plot ~= 16
	discBBO = BBO;

	load(['result_' res1]);
	CdtsEn = bestcandidate;
	bbosEn = bBBOs(:, end);
	bboinit = bBBOs(:,1);
	%
	load(['result_' res2]);
	CdtsLo = bestcandidate;
	bbosLo = bBBOs(:, end);
	bboinit = [bboinit; mean(bBBOs(:,1))];
	bboinit = mean(bboinit);
end

for i = 1:2

figStyl(i) = figure('position', pos);

% [tri, crossingTri, x1LinClean, x2LinClean, toPlotLinClean] =...
% 	dataSurface4Plot(Candidates, discBBO);
[XX, YY] = meshgrid(Candidates{1}, Candidates{2});

if fig2plot ~= 16
	discBBOPure = discBBO;
	discBBO(discBBO == 0) = NaN;
	ZZ = reshape(discBBO, size(XX))';
else	
	intonation2disp = intonation;
	load([dataPath '/results/result_AVECCRIT' Problem.name]);
	intonation2disp(BBO == 0) = 0;
	intonation2disp(intonation2disp == 0) = NaN;
	ZZ = reshape(intonation2disp, size(XX))';
end
surf(XX  .* 1e3, YY .* 1e3, ZZ);
view(0,90)
if i == 1
	colormap('jet');
else
	colormap('gray');
end
% trisurf(tri,x1LinClean,x2LinClean,toPlotLinClean);

hold on
if fig2plot ~= 16
	plot3(0, 0.00185 * 1e3, bboinit + 1.5, 'xk',...
		'markersize', 30, 'linewidth', 5);
end
if fig2plot == 8 % pour g�rer l'affichage de la contrainte

	[tri, crossingTri, x1LinClean, x2LinClean, ~] =...
	dataSurface4Plot(Candidates, discCons);

	[~,~,~,~, toPlotLinClean] =...
	dataSurface4Plot(Candidates, discBBOPure);
	trisurf(crossingTri, x1LinClean * 1e3, x2LinClean * 1e3, toPlotLinClean+0.01,...
		'FaceColor', 'k');
% 		'FaceColor', 'k','FaceAlpha', 0.4);
end
cb = colorbar('Fontsize', FontSizeTicks, 'FontName', FontName);

if fig2plot ~= 16
	c = cLo;
	if i == 2
		c = c2g(c);
	end
	p1 = plot3(CdtsLo(:, 1) * 1e3, CdtsLo(:, 2) * 1e3, bbosLo(:, end)+2.5, 'o',...
		'Color', c, 'markersize', MkSize);
	drawnow;
	c = get(p1,'color');
	c = 1 - 0.5*(1-cLo);
	if i == 2
		c = c2g(c);
	end
	% 	mp1 = p1.MarkerHandle;
	% mp1.get
	% pause(0.5);
	set(p1,'markerfacecolor',c);
	% 	mp1.FaceColorData = uint8(255*[c,alpha]');
	c = cEn;
	if i == 2
		c = c2g(c);
	end
	p2 = plot3(CdtsEn(:, 1) * 1e3, CdtsEn(:, 2) * 1e3, bbosEn(:, end) + 2.57, 'vr',...
		'Color', c, 'markersize', MkSize);
	drawnow;
	c = get(p2,'color');
	
	c = 1 - 0.5*(1-cEn);
	if i == 2
		c = c2g(c);
	end
		
% 	mp2 = p2.MarkerHandle;
	set(p2,'markerfacecolor',c);
% 	mp2.FaceColorData = uint8(255*[c,alpha]');
else
	%valeurs repris semi automatiquement
	plot3(0, 2.16, 3.9, 's','markerfacecolor', [0.8 0.8 0.8],'markersize', MkSize);
end
grid off
xlim([0, 1.59]);
ylim([1.4, 2.21]);
xlabel('cup depth (mm)');
ylabel('throat radius (mm)');

if fig2plot == 6
	legend([p1,p2], {' LOWESS ', ' Ensemble'},'FontSize',FontSize','FontName',...
		FontName);
	% 		FontName, 'position', [0.480 0.637 0.4 0.268]);
elseif fig2plot == 16
	cb.Label.String = 'Intonation (cents)';
	cb.Label.FontName = FontName;
	cb.Label.FontSize = FontSize;
	ax = get(gcf,'Children');
	ax(2).Position = [0.123 0.135 0.705 0.8150];
elseif fig2plot == 8
	cb.Label.String = 'SC value (n� harm)';
	cb.Label.FontName = FontName;
	cb.Label.FontSize = FontSize;
	legend([p1,p2], {' LOWESS ', ' Ensemble'},'FontSize',FontSize','FontName',...
		FontName, 'location', 'southeast');
elseif fig2plot == 18
	caxis([-0.0107 0.0846])
	cb.Label.String = 'SC value (n� harm)';
	cb.Label.FontName = FontName;
	cb.Label.FontSize = FontSize;
	legend([p1,p2], {' LOWESS ', ' Ensemble'},'FontSize',FontSize','FontName',...
		FontName, 'location', 'southeast');
end
end

fig = figStyl(1);
figBW = figStyl(2);
end
