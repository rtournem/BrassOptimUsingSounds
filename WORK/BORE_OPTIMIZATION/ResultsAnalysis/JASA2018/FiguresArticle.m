clear all
FontName = 'Times New Roman';
global ROOT cLo cEn
ROOT = '../../../../';
path(path, genpath([ROOT 'REDUNDANT']));
path(path, genpath('/Ressources'));
DataPath = [ROOT 'DATA/optim/JASA2018/'];
path(path,genpath([ROOT 'WORK/BORE_OPTIMISATION/BBoxDisc/Ressources']));
FontColor = [0, 0, 0];
cLo = [0, 0, 0.4];
cEn = [1, 0.3, 0];
%1: Figure 1 l'impedance (bug inimportant parce que pas de figure BW (deja BW)
%7: figure 7 + nbFigBastien hist plot 2D intonation
%9: figure 9a + nbFigBastien (colonne) hist plot 2D SC average
%19: figure 9b + nbFigBastien (colonne) hist plot 2D SC dynamics
%29 figure 9a + nbFigBastien (ligne)
%39 figure 9b + nbFigBastien (ligne)
%10 figure 10a + nbFigBastien hist plot 10D intonation
%20 figure 10b + nbFigBastien hist plot 10D SC average
%30 figure 10c + nbFigBastien hist plot 10D SC dynamics
%11 figure 11 + nbFigBastien best bores 10d
%12 figure 12a + nbFigBastien kmeans intonation
%22 figure 12b + nbFigBastien kmeans SC average
%32 figure 12c + nbFigBastien kmeans SC dynamics
%6: Figure 6a + nbFigBastien surface intonation
%16: Figure 6b + nbFigBastien surface intonation impedance
%8:  Figure 8a + nbFigBastien surface SC average
%18:  Figure 8b + nbFigBastien surface SC dynamics

for fig2plot = [6];
close all

switch fig2plot
	case 1
		%figure 1
		% lancer le code FromGeom2MGuides une fois avant pour avoir les variables pour
		% pouvoir tracer l'imp?dance.
		load('impedanceInitiale');
		namefig = 'impedance';
		pos = [200, 200, 1100, 400];
		fig = figure('Position', pos);
		FontSize = 30;
		FontSizeTicks = 20;
		plot(f,abs(ZTransMatNew),'k','linewidth', 2);
		text(213, 6.8e7, '2', 'Color', 'r');
		text(327, 7.6e7, '3', 'Color', 'r');
		text(444, 10.1e7, '4', 'Color', 'r');
		text(570, 12.1e7, '5', 'Color', 'r');
		xlabel('frequency (Hz)');
		ylabel('$|Z| \; (k\Omega )$','interpreter', 'latex');
	case {7, 9, 19, 29, 39, 10, 20, 30}
		%figure 7: history plot 2-d intonation
		[fig, FontSize, FontSizeTicks, namefig, figBW] = histplot(fig2plot, FontName);
	case 11
		[fig, FontSize, FontSizeTicks, namefig, figBW] = bestbores();
	case {12, 22, 32}
		[fig, FontSize, FontSizeTicks, namefig, figBW] = kmeansplot(fig2plot);
	case {6, 16, 8, 18}
		[fig, FontSize, FontSizeTicks, namefig, figBW] = surfaceplot(fig2plot, FontName);
		
end

for figStyl = [fig, figBW]
	
	figure(figStyl);
	% ax = ancestor(hlow, 'axes');
	ax = gca;
	% set(ax,'color','none')
	yrule = ax.YAxis;
	yrule.FontSize = FontSizeTicks;
	yrule.FontName = FontName;
	xrule = ax.XAxis;
	xrule.FontSize = FontSizeTicks;
	xrule.FontName = FontName;
	box off
	
	set(findall(findall(0,'Type','figure'),'type','text'),'Color', FontColor);
% 	set(findall(findall(0,'Type','uicontrol'),'style','text'),'FontSize',FontSize,...
% 		'fontName',FontName, 'Color', FontColor);
	
	set(gcf,'Units','centimeters');
	screenposition = get(gcf,'Position');
	set(gcf,...
		'PaperPosition',[0 0 screenposition(3:4)],...
		'PaperSize',[screenposition(3:4)],...
		'PaperUnits', 'centimeters');
	% set(fig, 'InvertHardCopy', 'off');
	if figStyl == fig
		print(['../../../../REC_GRAPH/JASA2018/Article/' namefig],'-dpdf','-painters');
	elseif figStyl == figBW
		print(['../../../../REC_GRAPH/JASA2018/Article/' namefig '_BW'],'-dpdf','-painters'); %-deps -deps2 -ps -ps2 -dbmpmono

	end
end
end







