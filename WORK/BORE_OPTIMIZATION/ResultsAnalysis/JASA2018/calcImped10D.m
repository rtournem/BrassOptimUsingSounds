function [intonation, Z] = calcImped10D(candidate)
global ROOT
path(path,genpath([ROOT 'WORK/IMPEDANCE_ANALYSIS']));
path(path,genpath('Ressources/'));

%% PROBLEM DEFINITION
%The instrument tessiture, this stands for the Bb trumpet only!
InsNotes = {{'E2','Bb3','F4','Bb4','D5','F5','Ab5','Bb5'};
	{'D2','Ab3','Eb4','Ab4','C5','Eb5','Gb5','Ab5'};
	{'Eb2','A3','E4','A4','Db5','E5','G5','A5'};
	{'C2','Gb3','Db4','Gb4','Bb4','Db5','E5','Gb5'}};

toolboxModel = 'Tournemenne';
load([ROOT 'DATA/impedance/ZModelisationTournemenne/LP10']);
TMMs = Mguides;
geom = instru;

%% PROGRAM PARAMETERs
Fing=1; %the settings are for a trumpet. If you want to modify the instrument you have to modify valueSet in ETDsCalc.m
%1=D0 (no valve pressed), 2=D1 (first valve pressed), 3=D2 (second valve pressed), 4=D23
Notes=[2,3,5]; %don't put the reference note
Ref=4;
Threshold=[]; %Threshold=[-20,+50]; %empty vector to avoid using it %these thresholds corresponds to acceptance limit for inharmonicity (in practise it is hard for a musician to increase the note of a pitch while it is easy to lower it)
MusicalMetric=0; %take into account the recurrence of a note in the musical pieces.
geomFinal = geom;
for i = 1 : length(candidate)
	geomFinal(1+i).rayon(2) = candidate(i);
	geomFinal(2+i).rayon(1) = candidate(i);
end

Fmax = 8000; %highest frequency where Z will be defined
N = 2^15;     %number of frequency samples
f = (1 : N) * Fmax / N;
Ta = 27;
To = 273.16;
T = To + Ta; % temperature avec Ta=masque(3,1);

Co=331.45*sqrt(T/To); % sound speed
ro=1.2929*(To/T); % air density

% 	[Z,f,~]=MT_Z(Geom1D,f,Co,ro,Ta,param_visc,show,0);
% 	Z=[f',real(Z'),imag(Z')];

disp('------ impedance en cours de calcul -------');
[Z, f] = MT_Z_RT(geomFinal, f, Co, ro, Ta, 10, 0);
disp('------ impedance calculee ------');
Z = [f',real(Z.'),imag(Z.')];


Fpeaks = peaksHolesSearch(Z(:, 1), Z(:, 2) + 1i * Z(:, 3), 5);

% 	Z(:,2)=Z(:,2).*(ro*Co/(pi*Geometry.r{1}(1)^2));
% 	Z(:,3)=Z(:,3).*(ro*Co/(pi*Geometry.r{1}(1)^2));
% 	[Z,Fpeaks]=ImpedCalc(Geometry,xs,k,TransLineMethod,max([Notes,Ref]));
%%%%%%%%%%%%% checking that the impedance and the peak frequency detection are sane
% 	figure;
% 	plot(Z(:,1),abs(Z(:,2)+1i*Z(:,3)),'b');
% 	hold on;
% 	plot(Fpeaks,6*10^7.*ones(size(Fpeaks)),'*r');
%%%%%%%%%%%%%

ETDs = ETDsCalc(InsNotes, Fpeaks, Fing, Notes, Ref);

intonation = IntonationCalc(InsNotes, ETDs, Threshold, MusicalMetric,...
	max([Notes, Ref]), Fing);

end
