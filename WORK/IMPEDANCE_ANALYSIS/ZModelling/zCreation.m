function [imped,freqs,holes]=zCreation(impedNFO)
% function managing the impedance calculation

global ROOT
if isempty(ROOT)
	ROOT='../../../';
	path(path,genpath('../'));
end
optionNFO.SaveFig=[ROOT 'REC_GRAPH/'];

if strcmp(impedNFO.toolboxModel,'Tournemenne')
	impedNFO.Fmax = 8000; %highest frequency where Z will be defined
	impedNFO.N = 2^15;     %number of frequency samples
	impedNFO.f = (1:impedNFO.N)*impedNFO.Fmax/impedNFO.N;
	impedNFO.param_visc=10;%Z creation parameter
	
	if isfield(impedNFO,'PeakEstim')
		optionNFO.PeakEstim=impedNFO.PeakEstim;
	else
		optionNFO.PeakEstim=1;
	end
	
	%actual computation
	To=273.16;
	T=To+impedNFO.Ta; % temperature avec Ta=masque(3,1);
	Co=331.45*sqrt(T/To); % vitesse du son
	ro=1.2929*(To/T); % densite
	
	%Impedance calculation
	if strcmp(impedNFO.toolboxModel,'Tournemenne')
		%the imped needs unit. If you want to check, look at the code of resonator.m
		[Z,f]=MT_Z_RT(impedNFO.geom,impedNFO.f,Co,ro,impedNFO.Ta,impedNFO.param_visc,0);
		imped=[f',real(Z.'),imag(Z.')];
	end
	if optionNFO.PeakEstim
			[freqs,~,~,~,holes,~,~]=peaksHolesSearch(imped(:,1),imped(:,2)+1i*imped(:,3),impedNFO.RegMax);
	else
		freqs=0;holes=0;
	end
	
else
	error('z calculation method inexistant');
end
end
