%%
clear variables global;
warning off; %#ok<WNOFF>
close all;
global ROOT
%addition of the needed path to get access to every required function
ROOT='../../';
path(path,genpath([ROOT 'WORK/FREQUENTIAL_SIMULATION/Ressources']));
path(path,genpath([ROOT 'WORK/IMPEDANCE_ANALYSIS']));
%% THE SWITCHES
%the following lines are to be set to 1 if you want to launch the corresponding
%process. Their order corresponds to the logical order: test, threshold
%obtention, coarse exploration, refined exploration and distribution estimation  

%Part 0: test of the design space
orderTestProb = 0;
option.drawTest = 0;
%Part 1: computation of the "power" threshold thanks to a standard geometry
orderThresh = 0;
option.drawThresh = 0;
%Part 2: corse exploration of the design space + musician param (pm, mua, fa)
orderStart = 0;
%Part 3: refined exploration + distribution limits estimation.
orderRefinAndSeg = 0;
option.drawNeigh = 0;
%Final Action: exporting file for the optimization process
expFiles = 0;
%Appendice 1
toDraw = 0; %I want to draw (need note.Taken (Part 2 executed))
%Appendice 2
%merges two note variables from two files and save only the note variable in a
%new file.
mergeFiles = 0;
%% Global parameters
%The technical name of the design problem we want to create and work on. 
%Unfortunately, since it is a experimental code,
%the only design problems that may work are the current one usually...
param.type = 'MP2TMM'; %LP10 MP2TMM
%the place where every result will be saved
param.pathResults=[ROOT 'DATA/HarmBal/Results/Batch/' param.type];

%Number of Partials for the frequential simulation (Harmonic balance technique):
param.N = 6;
%Parallel computing management: if you have the Parallel Computing Toolbox you
%can use as many (logical) CPUs as you have. If you don't have the toolbox then put 1.
%Unfortunately, unless to test the code, it will not be enough to use only one
%core for the entire process.
option.Workers = 22;

%%                 Initial test of the design problem
%The idea is to test some geometry of the design space to observe the eagerness
%of the harmonic balance technique to simulate some sounds.
%either you give directly some candidates to test or you provide the grid on
%which the candidates will be tested
% param.TestCdts=[0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464;
% 	0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825;];
% % Candidates = {[0, 0.003, 0.006],[0.001, 0.00185, 0.0025]};
% % %for the for loop contain all combination describing everycalculated points in the design space.
% % xspacePoints = allcomb(Candidates)';
% % param.TestCdts = xspacePoints';
param.TestCdts=[0.0001 0.00185];

%You can test several couple pm, mu
param.TestMasks = [5000 -1 ;10000 -2];
%all the fas you want to simulate, this parameter is directly linked to the
%notes that will be simulated. A careful choice is needed.
param.Testfas = 100:2:670;
%to plot the fplay/flip and reg 6 considerations
option.morePlots = 1;
option.dataVisuTest = [param.pathResults '/Tests/MP2TMM_2019_1_27_12_56_22_tests'];

%%          "power" Threshold identification thanks to standard geometry
%The idea is to study playing frequency variance for a lot of different sounds
%simulated using a standard instrument. Taking as reference known variance when
%played by real musician, the user will manually set looking at graphs obtained
%by these simulated sounds the percentage of set of parameters he wants to keep.
%The "power" threshold will be deduced from this percentage.

%the dynamics range you want to study (p, mp, mf, ff)
%ascending order; Pm range (don't place holes in the Pm range otherwise you will
%simulate sounds for nothing.
param.Pm2keep = [5000 9000;9000 12000];
%the mass per area of the lips range
param.muas = [-4 -0.5];% muas range
%the range of resonance frequency of the lips you want to study (directly
%related to the regime you want to study)
param.fas = [100, 670];% fas range
%the regime of interest
param.Regimes = 2:5; %has to be in accordance with fas in the exploration phase
%sometimes for the same set of parameters 2 different sound simulations can be
%obtained. Their is a strategy to only select the good one.
param.BisolRemoving = 1;% solution not unique, has to be cleaned.
%the standard geometry which should be comparable to one from the industry.
param.xstandard = [0,0.001825];% the geometry used to get thresholds
%the number of set of parameters pm, mua, fa you want to try to simulate.
param.NbPts2Sim4StandardGeometry = 200 * option.Workers; %the number of simulations
%if the step went well, you may want to load directly data instead of doing
%again some steps. The saved data are in the subfolder Protection
%param.ProtectionFile = [param.pathResults ...
%	'/Protection/ProtectingFindThreshPostCleaning_2018_3_11_12_56_58'];
param.ProtectionFile = [];
%you have to set the right file to load for data visu
option.dataVisuThresh = [param.pathResults...
	'/Raw/notes_MP2TMM_2019_1_27_19_7_5_xStand'];
%%                     First coarse Batch
%first exploration of the entire parameter space: geometrical parameters + pm,
%mua, fa.

%choice of the number of point to sample
param.NbPts2Sim = 500 * option.Workers;
%design space definition:
param.xs = [0,0.0025;0.0014,0.0022]; %design space definition
% if you did the thresholding step you will have all the notes from the standard
% geometry in note + the thresholds.
param.PreviousStep = [param.pathResults...
	'/Selected/notes_MP2TMM_2019_1_27_19_12_58_xStandSelected'];
%%
%this file will be used if you want to define your theshold thanks to another
%design problem. For example, a design problem sharing the same initial geometry
param.thresholdPath = [ROOT 'DATA/HarmBal/Results/Batch/' 'MP2TMM'...
	'/Thresholds/MP2TMM_2018_3_12_15_0_4_thresholds'];
%%                      Final refinement
% the previous coarse exploration gives a good idea of every subset of the
% entire parameter space that simulates the corresponding notes above the
% "power" thresholds. Based on the limit found we undertake another refined
% exploration of each subse. 

%number of points to simulate for every note and every dynamics range
param.Points2SampleRefin = 300 * option.Workers;

%Number of closest neighbor used to build the threshold distance.
param.neighbor = 4;
%percentile used to define the threshold distance.
param.percentileFullspace = 75;
param.percentileConception = 95;

%Warning this could be a file with coarse points OR file with refined points
%which need to be segmented.
% param.PreviousStep = [param.pathResults '/Refined/notes_LP10_2018_3_31_8_56_28_refin_notes_LP10_2018_4_2_10_14_21_refin_Merged'];
param.PreviousStep = [param.pathResults '/Selected/notes_MP2TMM_2019_1_27_19_39_24_coarseselected.mat'];

%%                          Export
%this final step creates the file used by the actual optimization process.

%Name used by the optimization program.
param.SetName = 'MP2TMM_2_3idxPM_D0';
%you have to feed a file obtained after the refined process (the only one having
%every required information.
param.toExport = [param.pathResults '/Refined/notes_MP2TMM_2_2018_8_30_3_13_36_dist'];
%%                           Draw
param.SaveFig = 1;

%%                            PROCESSINGS
tic;
%if you start to work on a new design problem, all the required folders are created.
if ~exist(param.pathResults,'dir')
	mkdir([param.pathResults '/Tests']);
	mkdir([param.pathResults '/Protection']);
	mkdir([param.pathResults '/Raw']);
	mkdir([param.pathResults '/Selected']);
	mkdir([param.pathResults '/Refined']);
	mkdir([param.pathResults '/Thresholds']);
end
%Loading the required file if you want to do teh coarse or refined exploration
%of the entire parameter space.
if orderStart | orderRefinAndSeg
	if isempty(param.PreviousStep) & orderStart
		disp('Warning: you loaded a special threshold data file for this step!');
		note.taken=cell(1,size(param.Pm2keep,1));
		note.powTaken=cell(1,size(param.Pm2keep,1));
		load(param.thresholdPath);
		note.Thresholds = thresholds.val;
	elseif strfind(param.PreviousStep, param.type)
		disp('Warning: you loaded a data file for this step!');
		load(param.PreviousStep,'note');
	else
		error('There is a problem with data files');
	end
end
% management of the parallel processing
if option.Workers > 1 & ~option.drawThresh & ~option.drawTest & ~toDraw
	poolSimu=gcp('nocreate');%if no worker opened.
	if isempty(poolSimu)
		parpool(option.Workers);
	elseif poolSimu.NumWorkers~=option.Workers %if the number of opened worker is not right.
		delete(gcp('nocreate'))
		parpool(option.Workers);
	end
end
%computation part dedicated to the test of the design space prior to any
%ressource intensive exploration step.
if orderTestProb
	if option.drawTest
		load(option.dataVisuTest);
		testSimuAna(param, testResults, option);
	else
		[testResults, param] = testProbConv(param, option);
		save([param.pathResults '/Tests/' param.type clockFormat '_tests'],...
			'testResults', 'param', 'option');
	end
end
% "power threshold" computation from standard geometry
if orderThresh
	if option.drawThresh
		load(option.dataVisuThresh);
		%We select thresholds manually here after visual graphs analysis
		[param, note, thresholds] = drawingandSelectingThresholds(param, note);
		save([param.pathResults '/Selected/' 'notes_' param.type clockFormat '_xStandSelected'],...
			'param','note','option');
		save([param.pathResults '/Thresholds/' param.type clockFormat '_thresholds'],...
			'thresholds');
	else
		%simulates notes
		[note, param] = findingThreshFromStandardGeom(param, option);
		save([param.pathResults '/Raw/' 'notes_' param.type clockFormat '_xStand'],...
			'param','note','option');
	end
end
% coarse exploration of the entire paramter space.
if orderStart
	[note, param] = range2Notes(param, note, option);
	save([param.pathResults '/Raw/' 'notes_' param.type clockFormat '_coarse'],...
		'param','note','option');
	[note, param] = cleaningNotes(param, note, option);
	save([param.pathResults '/Selected/' 'notes_' param.type clockFormat '_coarseselected'],...
		'param','note','option');
end
% refined exploration of the subset of every note and dynamics range and
% computation of the distance threshold
if orderRefinAndSeg
	if option.drawNeigh
		%distance threshold computation
		[note, param] = meanDist(param, note);
			save([param.pathResults '/Refined/' 'notes_' param.type clockFormat '_dist'],...
				'param','note','option');
	else
		%refined exploration
		[note, param] = refiningExplor1MassMdl(param, note, option);
		save([param.pathResults '/Refined/' 'notes_' param.type clockFormat '_refin'],...
			'param','note','option');
	end
end
%%                              DRAWINGS
if toDraw
	param.fileName = fileName;
	plotRegimes(param, note);
end
%%                               EXPORTS
if expFiles
	load(param.toExport, 'note');
	optimCatalystCreation(param, note);
end
%%                             FILE MERGING
if mergeFiles
	mergingFiles;
end
%%                     EXTINCTION PARRALEL MODE
if option.Workers >  1 & ~ishandle(1) %checking if there is a figure... I think
	p = gcp('nocreate');
	delete(p)
end
toc;
