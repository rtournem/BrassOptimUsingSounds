function [isGood,freq,Pfinal,musicianNFO,instruNFO,simuNFO,physNFO,optimNFO]=...
	eqHarmo(musicianNFO,instruNFO,simuNFO,physNFO,optimNFO)
%function managing the entire harmonic balance technique.

path(path,genpath('Ressources'));
global Nn
global VERB
global DATA_PATH
if isempty(DATA_PATH)
	DATA_PATH='../../DATA/HarmBal/';
end

%%          PARAM
VERB=simuNFO.Verbose;
N=simuNFO.N; 
Nn=64;
simuNFO.Nn=Nn; %used to do the DFT and iDFT cleverly
simuNFO.Finit=simuNFO.Pi(1,N+3);
To=273.16;
T=To+physNFO.Ta; 
physNFO.Co=331.45*sqrt(T/To); 
physNFO.ro=1.2929*(To/T); 
instruNFO.Zc=physNFO.ro*physNFO.Co/(pi*instruNFO.r^2); 

optimNFO.delp=0.001;
optimNFO.Conv(1)=1;
optimNFO.Convf=1;       

k=1; 
isGood=1;
%the convergence criterion tries to see if partial are balanced AND the
%Playing Freq Stable
while (abs(optimNFO.Conv(k))>=10^(-optimNFO.EPS)) | (optimNFO.Convf>=10^(-2))
	if isnan(simuNFO.Pi(1,N+3))
		isGood=0;
		break;
	end	
	if VERB
		disp(' ')
		disp(['---------------------ITERATION No ', int2str(k),'-------------------------'])
	end
	
	simuNFO.Pi=piPerturbation(optimNFO,simuNFO.Pi,N);
	
	if VERB
		disp('Passage dans la boucle F(p)')
	end
	[Pf,musicianNFO]=threeEqModel(musicianNFO,instruNFO,simuNFO,physNFO);%Pf ne contient pas la fr?quence! mais la partie imaginaire de la phase de la fondamentale (edit 4/12/16).

	%%         CONVERGENCE
	optimNFO.Conv(k+1)=sqrt((sum((simuNFO.Pi(1,[1:N+2,N+4:end])-Pf(1,[1:N+2,N+4:end])).^2)+Pf(1,N+3)^2)...
		/sum(simuNFO.Pi(1,[1:N+2,N+4:end]).^2));
	
	if optimNFO.Conv(k+1)>10^3 | isnan(optimNFO.Conv(k+1))
		isGood=0;
		break;
	end
	
	if VERB
		disp(' ');
		disp('-----------------------------------------------------')
		disp(['	TEST DE CONVERGENCE : ',num2str(optimNFO.Conv(k+1),8)])
		disp('-----------------------------------------------------')
		disp(' ')
	end
	if (k>1)
		optimNFO.Convf=abs(FREQ(2)/FREQ(1)-1);
	else
		optimNFO.Convf=0;
	end
	
	if (abs(optimNFO.Conv(k+1))<10^(-optimNFO.EPS)) & (optimNFO.Convf<10^(-2))
		break;
	else
		[simuNFO.Pi,FREQ,XJg]=incr_NR(simuNFO.Pi,Pf,N,optimNFO.delp);  
		if VERB
			disp('Calcul de l''increment')
			disp(['old / new freq:    ' num2str(FREQ)]);
			
		end
		if FREQ(2)<simuNFO.Finit-40 | FREQ(2)>simuNFO.Finit+40 | FREQ(2)<instruNFO.imped(1,1)
			isGood=0;
			break;
		end
		Valeur_propre(:,k)=eig(XJg);
	end
	k=k+1;
	if (k>optimNFO.Itermax)
		isGood=0;
		if VERB
			disp(['TROP GRAND NOMBRE D''ITERATIONS  (',int2str(k),') OU DIVERGENCE']);
		end
		break;
	end
end

if exist('FREQ','var')
	freq=FREQ(2);
else
	freq=simuNFO.Pi(1,N+3);
end
Pfinal=simuNFO.Pi(1,:);
%just for information
optimNFO.k=k;
end
