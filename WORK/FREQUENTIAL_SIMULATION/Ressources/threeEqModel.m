function [pf,musicianNFO]=threeEqModel(musicianNFO,instruNFO,simuNFO,physNFO)
%function managing the direct computation of the harmonic balance technique.

global VERB
%%      Y(jw)
Y=oneMassOsc(musicianNFO,simuNFO);

%%       y(t)
y=fourierinv(Y,simuNFO);   % y(2N+3,Nn) y=Y(continu)+som{2*Re(Y(n)exp(2pi*nft))}  

%%	   p(t)
if VERB
	disp('    Synthese additive de p(t)')
end
pi2p=simuNFO.Pi(:,1:simuNFO.N+1)+1i.*simuNFO.Pi(:,simuNFO.N+2:end);
pi2p(:,2)=real(pi2p(:,2));%because Im(p1)=0
p=fourierinv(pi2p,simuNFO);

%%	   non-linear function
if VERB
	disp('    Application de l''equation non lineaire')
end
u=nlFluidEq(y,p,musicianNFO,simuNFO,physNFO);                  %(2N+3,Nn)

%%		Frequential domain
if VERB
	disp('    Calcul des composantes harmoniques de u(t)')
end
U=temp2ProcessedFourier(u,simuNFO);

%%	    input impedance
if VERB
	disp('    Application de l''equation lineaire')
end
pf=resonator(U,instruNFO,simuNFO);  %Nu deleted

%%      Last Editing
musicianNFO.p=p(1,:);
musicianNFO.u=u(1,:);
musicianNFO.y=y(1,:);
