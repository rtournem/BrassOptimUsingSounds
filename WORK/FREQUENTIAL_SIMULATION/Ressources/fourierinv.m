function y=fourierinv(Y,simuNFO)
%going in the temporal domain for the non linear equation

N=simuNFO.N;
Nn=simuNFO.Nn;
y=[];
for n=1:2*N+3
	for ii=1:Nn
		y(n,ii)=real(Y(n,1));
		for k=2:N+1 
			y(n,ii)=y(n,ii)+2*real(Y(n,k))*cos(2*pi*(k-1)*(ii-1)/Nn)-2*imag(Y(n,k))*sin(2*pi*(k-1)*(ii-1)/Nn);
		end
	end
end
