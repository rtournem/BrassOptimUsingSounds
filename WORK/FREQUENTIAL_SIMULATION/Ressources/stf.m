function [Piext,pext]=stf(Pi,freq,N,XpDir)
% calculation of the pressure outside the instrument from the pressure in the
% mouthpiece. 

global Nn


if exist('XpDir','var')
	H12=load([XpDir '/H12.txt']);
	H13=load([XpDir '/H13.txt']);
	H23=H13./H12;
	H23(:,1)=H13(:,1);
	H23(:,2)=SignalMeanLifting(H23(:,2));
	H23(:,3)=SignalMeanLifting(H23(:,3));
	for n=1:N
		fn=n*freq;
		idxn=find(H23(:,1)<fn,1,'last');
		TF(n)=H23(idxn,2)+1i*H23(idxn,3);
	end
else
	Fc=1400 ;
	TF=[];
	for n=1:N
		fn=n*freq; 
		if fn<Fc
			TF(n)=(fn/Fc)*1 ; 
		else
			TF(n)=1 ;
		end
	end
end


% Real and imaginary parts of Cn correspondant ? pint(t), ReCint et ImCint, (1,N)
ReCint=Pi(1,2:N+1);
ImCint=Pi(1,N+3:2*N+2);
ImCint(1,2)=0; 
Cint=[];
Cint=ReCint+1i*ImCint; 

Cext=[]; % Cext (1,N)
Cext=TF.*Cint;


% Piext(1,2N+2) au format de Pi,
% ... Real and imaginary parts of Cn correspondant ? pext(t), ReCext et ImCext, (1,N)
Piext=[];
	Piext(1,1)=0 ;   % "continu" (real)=0
	Piext(1,N+2)=0 ; % "continu" (imag)=0
for n=1:N % somme sur les N harmoniques
   Piext(1,n+1)=real(Cext(n));
   Piext(1,N+2+n)=imag(Cext(n));
end

% synth?se additive de pext(t)
pext=[]; % pext(t) (Nn)
for ii=1:Nn
	   pext(ii)=0 ;
   for n=1:N % somme sur les N harmoniques ("continu"=0)
		pext(ii)=pext(ii)+2*real(Cext(n))*cos(2*pi*n*(ii-1)/Nn)-2*imag(Cext(n))*sin(2*pi*n*(ii-1)/Nn);
   end
end
function SignalLifted=SignalMeanLifting(Gain)

GainLowLow=[Gain(1);Gain(1);Gain(1:end-4);Gain(end);Gain(end)];
GainLow=[Gain(1);Gain(1);Gain(2:end-3);Gain(end);Gain(end)];
GainHigh=[Gain(1);Gain(1);Gain(4:end-1);Gain(end);Gain(end)];
GainHighHigh=[Gain(1);Gain(1);Gain(5:end);Gain(end);Gain(end)];
SignalLifted=(Gain+GainLow+GainLowLow+GainHigh+GainHighHigh)./5;


end
