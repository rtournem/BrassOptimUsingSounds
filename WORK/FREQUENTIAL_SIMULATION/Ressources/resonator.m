function Pf=resonator(U,instruNFO,simuNFO)
%instrument equation: input impedance.

N=simuNFO.N;
f1=simuNFO.Pi(1,N+3);
f2=simuNFO.Pi(N+4,N+3); 
imped=instruNFO.imped;
k=1;
for j=2:(N+1)  %j=1:N+1
	for i=k:size(imped,1)-1
		if imped(i,1)<=(j-1)*f1 & imped(i+1,1)>(j-1)*f1
			
			Z1=imped(i,2)+imped(i,3)*1i;
			Z2=imped(i+1,2)+imped(i+1,3)*1i;
			Z(j)=Z1+(Z2-Z1)*((j-1)*f1-imped(i,1))/(imped(i+1,1)-imped(i,1));
			Z3=Z1+(Z2-Z1)*((j-1)*f2-imped(i,1))/(imped(i+1,1)-imped(i,1));
			k=i+1;
			break
		end
	end
	
	try
		Zcor=Z(j)*ones(2*N+3,1);
	catch ME
		if strcmp(ME.identifier, 'MATLAB:UndefinedFunction')
			error(['the required frequency is not covered by the input impedance'...
				' curve available: f = ' num2str(f1)])
		else
			error('we don''t know what happpened, look into resonator.m');
		end
		
	end
		Zcor(N+4)=Z3;	
		ploum=Zcor.*(U(:,j)+U(:,j+N+1)*1i);
		Pf(:,j)=real(ploum);
		Pf(:,j+N+1)=imag(ploum);
	end
	
