function plotRegimes(Param,Note)
% used to observe if needed the distribution of points during the stages of the
% preprocessing. 
 
global ROOT
color={'b','g','k','r','c','m','y','b'};
for idxpm=1:length(Note.Taken)
	ggg=figure('Name','All retained point; MP Depths by color');
	scatter3(Note.Taken{idxpm}(:,1),Note.Taken{idxpm}(:,2),Note.Taken{idxpm}(:,3),...
		30,sum(Note.Taken{idxpm}(:,end-size(Param.xs,1)+1:end).^2,2),'filled');
	cameramenu;
	hh=figure('Name','All retained point by regime');
	% dd=figure('Name','convex hull (4D) projected on 3D');
	cc=figure('Name','sol repartition throughout MP Depth');
	bb=figure('Name','mean spectrum (also over Pm) by regime');
	dd=figure('Name','Fplay according to Flip');
	for reg=Param.Regimes
		%drawing of the solutions in each regime, color represent the MP depth
		%the cube represent the refinement made in the third step of the
		%simulation
		figure(hh);
		subplot(2,3,reg);
		hold on;
		TakenReg=Note.Taken{idxpm}(Note.Taken{idxpm}(:,end-size(Param.xs,1))==reg,:);
		scatter3(TakenReg(:,1),TakenReg(:,2),TakenReg(:,3),30,...
			sum(TakenReg(:,end-size(Param.xs,1)+1:end).^2,2),'filled');
		cameramenu;
		figure(cc);
		subplot(2,3,reg);
		title(['regime: ' num2str(reg)]);
		hold on;
		%we have to be shure that the 20% are well dipastched in on the Xs
		%axes. Otherwise it means than a part of our geometrical design space
		%cannot converge.
		if size(Param.xs,1)==1
			hist(TakenReg(:,end),20);
		elseif size(Param.xs,1)==2
			hist3(TakenReg(:,end-1:end),[20 20]);
			cameramenu;
		else
			GeomVar=TakenReg(:,end);
			%normalizing
			for i=1:size(GeomVar,2)
				GeomVar(:,i)=GeomVar(:,i)-min(GeomVar(:,i));
				GeomVar(:,i)=GeomVar(:,i)./max(abs(GeomVar(:,i)));
			end
			hist(sum(GeomVar.^2,2),20);
		end
		figure(bb);
		subplot(2,3,reg);
		MeanNoteReg=mean(TakenReg);
		Magnitude=abs(TakenReg(:,5:5+6)+1i*TakenReg(:,6+6:2*6+6));
		StdNoteReg=std(Magnitude);
		bar(abs(MeanNoteReg(5:5+6)+1i*MeanNoteReg(6+6:2*6+6)));
		hold on
		plot([1 2 3 4 5 6 7],StdNoteReg,'r*');
		figure(dd);
		scatter(TakenReg(:,3),TakenReg(:,4),4,sum(TakenReg(:,end-size(Param.xs,1)+1:end).^2,2),'filled');
		hold on;
	end
	if Param.SaveFig
		%Saving everything
		% 	savefig(gg,[ROOT 'REC_GRAPH/3Slices_' Param.FileName '_' clockFormat '.fig']);
		% 	figure(gg);
		% 	print([ROOT 'REC_GRAPH/3Slices_' Param.FileName '_' clockFormat '.png'],'-dpng');
		savefig(ggg,[ROOT 'REC_GRAPH/AllSols_' Param.FileName '_' clockFormat '.fig']);
		figure(ggg);
		print([ROOT 'REC_GRAPH/AllSols_' Param.FileName '_' clockFormat '.png'],'-dpng');
		savefig(hh,[ROOT 'REC_GRAPH/AllSolsByReg_' Param.FileName '_' clockFormat '.fig']);
		figure(hh);
		print([ROOT 'REC_GRAPH/AllSolsByReg_' Param.FileName '_' clockFormat '.png'],'-dpng');
		% savefig(dd,[ROOT 'REC_GRAPH/ConvexHulls_' Param.FileName '_' clockFormat '.fig']);
		% figure(dd);
		print([ROOT 'REC_GRAPH/ConvexHulls_' Param.FileName '_' clockFormat '.png'],'-dpng');
		savefig(cc,[ROOT 'REC_GRAPH/PowByMPDepth_' Param.FileName '_' clockFormat '.fig']);
		figure(cc);
		print([ROOT 'REC_GRAPH/PowByMPDepth_' Param.FileName '_' clockFormat '.png'],'-dpng');
		savefig(bb,[ROOT 'REC_GRAPH/MeanSpectrum_' Param.FileName '_' clockFormat '.fig']);
		figure(bb);
		print([ROOT 'REC_GRAPH/MeanSpectrum_' Param.FileName '_' clockFormat '.png'],'-dpng');
		savefig(dd,[ROOT 'REC_GRAPH/FPLAYVSFlip_' Param.FileName '_' clockFormat '.fig']);
		figure(dd);
		print([ROOT 'REC_GRAPH/MeanSpectrum_' Param.FileName '_' clockFormat '.png'],'-dpng');
	end
end
end
function Slices=SlicingBatch(Params,xs,Width,Regimes)
if iscell(Params)
	for i=Regimes
		NCircleRadius=sum(Params{i}(:,4:end).^2,2);
		%the following is wrong, correct it if needed!
		Slices{i,1}=find(NCircleRadius<=xs(1)+Width*(xs(2)-xs(1)));
		Slices{i,2}=find(NCircleRadius>=(xs(2)+xs(1)-Width*(xs(2)-xs(1)))/2 &...
			NCircleRadius<=(xs(2)+xs(1)+Width*(xs(2)-xs(1)))/2);
		Slices{i,3}=find(NCircleRadius>=xs(2)-Width*(xs(2)-xs(1)));
	end
else
	NCircleRadius=sum(Params(:,4:end).^2,2);
	Slices{1}=find(NCircleRadius<=sum((xs(:,1)+Width.*(xs(:,2)-xs(:,1))).^2));
	Slices{2}=find(NCircleRadius>=sum(((xs(:,2)+xs(:,1)-Width*(xs(:,2)-xs(:,1)))/2).^2) &...
		NCircleRadius<=sum(((xs(:,2)+xs(:,1)+Width*(xs(:,2)-xs(:,1)))/2).^2));
	Slices{3}=find(NCircleRadius>=sum((xs(:,2)-Width*(xs(:,2)-xs(:,1))).^2));
end
end
