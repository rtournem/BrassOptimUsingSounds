function 	[note, param] = meanDist(param, note)
%this routine determines the maximal distance value (threshold distance) between
%points in the parameter space above which a set of parameters are discarded.
%Two distances are computed in two different parameter space. The parameter
%space generated only by the geometrical variables, and the full parameter space
%considering the geometrical variables and Pm, mua, fa.

%The distance considered is the distance to the 4th neighbor. During the
%optimization, if the distance between the incumbent set of parameters and the
%4th neighbor found in the corresponding parameter space is higher than the
%distance found by this routine, the set of parameters is discarded. 

%In order to find these threshold distances (for every range of dynamics "idxpm"
%and considered note "reg"), we compute the pairwise distances of every viable set of
%parameters found during the coarse and refined explorations. The parameter
%points from the standard geometry used to find the "power" thresholds are not
%taken into account to keep the results balanced. Once these distances are
%computed, the discrete distribution obtained for the distance to the fourth neighbor is
%considered up to a certain percentile (param.percentileFullspace or
%param.percentileConception), and the retained theshold distance corresponds to
%the one of the percentile.

%you can customize the neighbor considered. I usually go for the 4th one. It
%avoids difficulties with stange parameter space distributions.
note.neighbor = param.neighbor;
N = param.N;
% double foor-loop: for every consider range of dynamics and every considered
% note.
for idxpm = 1:size(param.Pm2keep, 1)
	for reg = param.Regimes
		% we wll not consider the standard geometry used to get the "power"
		% threshold.
		if size(param.xstandard, 2) == size(note.taken{idxpm}, 2) - (7 + 2 * N) 
			note2Analyze = note.taken{idxpm}(note.taken{idxpm}(:,7+2*N) == reg &...
				~ismember(note.taken{idxpm}(:,8+2*N:end),param.xstandard,'rows'),:);
		else
			note2Analyze = note.taken{idxpm}(note.taken{idxpm}(:,7+2*N) == reg,:);
		end
		%these are every set of parameters that we have to analyze (rows=number of
		%sets, cols= every dimensions (Pm, mua, fa, geometrical parameters)
		param2Analyze = note2Analyze(:,[1:3, end - size(param.xs,1) + 1:end]);
		%Normalization of every dimension. This is tricky because of the fact that
		%the expected limits are not always reached.
		maxVal = repmat([param.Pm2keep(idxpm,2), param.muas(2), max(param2Analyze(:, 3)),...
			param.xs(:,2)'], size(param2Analyze(:, 1)));
		minVal = repmat([param.Pm2keep(idxpm,1), param.muas(1), min(param2Analyze(:, 3)),...
			param.xs(:,1)'], size(param2Analyze(:, 1)));
		param2AnalyzeNorm = (param2Analyze - minVal) ./ (maxVal - minVal);
		paramDesignSpace = param2AnalyzeNorm(:,4:end);
		
		%value storage for possible future analysis
		note.maxVal{idxpm, reg} = [param.Pm2keep(idxpm,2), param.muas(2), max(param2Analyze(:, 3)),...
			param.xs(:,2)'];
		note.minVal{idxpm, reg} = [param.Pm2keep(idxpm,1), param.muas(1), min(param2Analyze(:, 3)),...
			param.xs(:,1)'];
		
		%point2perc gives the threshold distance. The variable names' are quite
		%misleading: the user provides a percentile and the function points2perc
		%returns a threshold distance note.entirePercentiles(idxpm, reg) or
		%note.designPercentiles(idxpm, reg)
		[note.entirePercentiles(idxpm, reg), idxsorted] = points2perc(param.neighbor,...
			param.percentileFullspace, param2AnalyzeNorm, idxpm, reg, 'Entier');
		note.designPercentiles(idxpm, reg) = points2perc(param.neighbor,...
			param.percentileConception, paramDesignSpace, idxpm, reg, 'Conception');

	end
end
end
function [perc, idxsorted] = points2perc(neighborNumber, percentile, points,...
	idxpm, reg, label)
    % computes the distance between every points in the list (rows) points of
    % (cols dimensions).
		distNeigh = pdist(points,'chebychev');
		% stores these distances as a square matrix where i,j is the distance
		% between the point i and point j.
		distOrga = squareform(distNeigh);
		disp(label);
		disp('-------------------------');
		
		% sorts the watrix of distance row wise, meaning that on the line i you will
		% find ordere the distances between the point i and any other point of the
		% list points. 
		[distSorted, idxsorted] = sort(distOrga, 2);
		[~ , vect_tricks] = meshgrid(zeros(1,size(distOrga, 1)), 1 : size(distOrga, 1));
		idxsorted = sub2ind(size(distOrga), idxsorted(:), vect_tricks(:));
		
		%perc is the threshold distance at the corresponding percentile (very
		%misleading name, sorry). 
		perc = prctile(distSorted(:,neighborNumber + 1), percentile);
		%some plotting to observe if the neighbor number and the corresponding
		%percentile seems to understand well the distribution you are working with.
		figure('Name', [label ' - ' num2str(idxpm) ' - ' num2str(reg) ' - ' num2str(perc)]);
		boxplot(distSorted(:,1:30));
		hold on
		plot(repmat(1 : 5, size(distSorted,1), 1), distSorted(:,1:5),'^k');
		disp([num2str(percentile) 'th percentile for idxpm ' num2str(idxpm) ' at reg '...
			num2str(reg) ' is:' num2str(perc)]);
end
