function [nfo,param]=nfoGeneralParam(nfo,param)
%Parameters linked to low-level tuning. You should touch these parameters only
%for some specific task.

nfo.imped.RegMax=9; %number of peaks we wanna know the frequency position of
%-MUSICIAN
nfo.mask.Ql=3;
nfo.mask.H0=0.0001;
nfo.mask.b=0.01;
%-PHYSICS
nfo.Phys.Ta=27;
nfo.imped.Ta=nfo.Phys.Ta;
%-INSTRUMENT
nfo.Instru.r=0.008;
param.RadXP=0; %1: radiation fonction
if param.RadXP
	nfo.Instru.XpDir=[pathImpeds conf '/'];
end
nfo.imped.r=nfo.Instru.r;
%-SIMULATION
nfo.Simu.N=param.N;
%Simunfo.fe=44100; %used to create sounds -> useless in bash
nfo.Simu.ResEdit=0;
nfo.Simu.Verbose=0; %1 if you want to see how the convergence is going
%-OPTIMSIMULATION
param.strat={'OnlyFirstPartial','AllNeighbors_Depth_1','highBW',1}; %OnlyFirstPartial   %FirstAndSecondPartial  %Closer2reality %AllNeighbors_Depth_X %largeBW %lowBW %highBW  %LAST INPUT THE PRECISION (a scalar (in Hz))
nfo.Optim.EPS=6;%power of 10
nfo.Optim.Itermax=30; %useless to go to 600 we want to converge very rapidly, we don't want to bet on luck

end
