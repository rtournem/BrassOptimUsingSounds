function Pis=initVectHB(Mask,Candidate,AllNotes,fPeaks,holes,param)
%selection procedure for the initial vector at the basis of the harmonic balance
%calcultation. To understant this process, read the section 3.2.1 of my thesis.
%It is in french but pictures and tables should help.

%the experiment showed that a precision of 1Hz doesn't provide a far better
%convergence property. if 3Hz deteriorate the results reduce it to 2Hz
precision=param.strat{4};
N=param.N;

if Mask(3)<120 %to take into account the first regime 
	idxReg=find(fPeaks>Mask(3)-50,2,'first');
else
	idxReg=find(fPeaks>Mask(3),2,'first');
end
reg=1;
for i=idxReg
	idxendReg(reg)=find(holes>fPeaks(i),1,'first');
	reg=reg+1;
end
if strcmp(param.strat{3},'largeBW')
	WatchedRegimes=[holes(idxendReg-1)' holes(idxendReg)' ];
elseif strcmp(param.strat{3},'lowBW')
	WatchedRegimes=[holes(idxendReg-1)' fPeaks(idxReg)'+10];
elseif strcmp(param.strat{3},'highBW')
	%-10 for the notes slightly under the peak (shouldn't happen for a trumpet)
	WatchedRegimes=[fPeaks(idxReg)'-10 holes(idxendReg)' ];
end

Finits=[WatchedRegimes(1,1):precision:WatchedRegimes(1,2) WatchedRegimes(2,1):precision:WatchedRegimes(2,2)];

%Neighbor strat
Pis=partialCreation(Mask,Finits,N,param.strat);
NeighborStrat=strfind(param.strat{2},'AllNeighbors_Depth');
if NeighborStrat
	if ~isempty(AllNotes)	
		%computing the vector giving the distance between every already
		%found points and the current one
		depth=str2double(param.strat{2}(end));
		%you have to abs all the next line because mua is negative.
		DistMus=abs(AllNotes(:,1:3)-repmat(Mask,size(AllNotes,1),1));
		DistCand=abs(AllNotes(:,8+2*N:end)-repmat(Candidate,size(AllNotes,1),1));
		%normalizing the distances
		for i=1:3
			DistMus(:,i)=DistMus(:,i)./max(abs(AllNotes(:,i)));
		end
		for i=1:length(Candidate)
			DistCand(:,i)=DistCand(:,i)./max(abs(AllNotes(:,7+i+2*N)));
		end
		Dist=[DistMus ,DistCand];
		Dist=sqrt(sum(Dist.^2,2));
		[~,idx]=sort(Dist);
		%taking the five first neighbor
		NoteNeighbor=AllNotes(idx(1:min(size(AllNotes,1),depth*5)),:);
		if ~isempty(NoteNeighbor)
			Pis=[NoteNeighbor(:,5:6+2*N);Pis];
			Pis(1:size(NoteNeighbor,1),N+3)=NoteNeighbor(:,4);
		end
	end
end
end
