%merge files, which can be interesting in some situations

clear all
text =  'main file selection';
defpath = '../../DATA/HarmBal/Results/Batch';
[fileprinc, pathprinc] = uigetfile('*.mat', text, defpath);
[file2merge, pathsec] = uigetfile('*.mat', text, pathprinc);

load([pathprinc fileprinc]);
notePrinc = note;
load([pathsec file2merge]);
noteSec = note;

for i=1:3
	notemerged.taken{i} = [notePrinc.taken{i}; noteSec.taken{i}(...
		~ismember(noteSec.taken{i}(end - size(param.xs, 1) + 1 : end), param.xstandard, 'rows'), :)];
	notemerged.powTaken{i} = [notePrinc.powTaken{i}; noteSec.powTaken{i}(...
		~ismember(noteSec.taken{i}(end - size(param.xs, 1) + 1 : end), param.xstandard, 'rows'))];
end

notemerged.Thresholds = notePrinc.Thresholds;
notemerged.All = [notePrinc.All; noteSec.All];
notemerged.success = [notePrinc.success; noteSec.success];
notemerged.AllThresh = [notePrinc.AllThresh; noteSec.AllThresh];
notemerged.powThresh = [notePrinc.powThresh; noteSec.powThresh];
notemerged.AllThreshCleaned = [notePrinc.AllThreshCleaned; noteSec.AllThreshCleaned];
notemerged.AllThreshCleanedPow = [notePrinc.AllThreshCleanedPow; noteSec.AllThreshCleanedPow];

note = notemerged;

save([pathprinc fileprinc(1:end-4) '_' file2merge(1:end-4) '_Merged'],'note','param','option');
