function [note, param] = cleaningNotes(param,note,option)
% removing under the sounds under the threshold and finally the bi-solutions.

N = param.N;
%% selecting according to the thresholds!
noteTemp = note.All;
try
	powTemp = noteTemp(:,5:N+5)+1i*noteTemp(:,N+6:2*N+6);
catch
	for idxpm = 1:size(param.Pm2keep, 1)
		note.taken{idxpm} = [];
		note.powTaken{idxpm} = [];
	end
	return
end
powTemp = sqrt(sum(abs(powTemp).^2,2));
powTemp= powTemp./noteTemp(:,1);

note.AllThresh = [];
note.powThresh = [];
for idxpm = 1:size(param.Pm2keep, 1)
	noteIdxPm = noteTemp(noteTemp(:, 1) > param.Pm2keep(idxpm, 1) &...
		noteTemp(:, 1) < param.Pm2keep(idxpm, 2), :);
	powIdxPm = powTemp(noteTemp(:, 1) > param.Pm2keep(idxpm, 1) &...
		noteTemp(:, 1) < param.Pm2keep(idxpm, 2));
	for reg = param.Regimes
		noteRegime = noteIdxPm(noteIdxPm(:,7+2*N) == reg, :);
		powRegime = powIdxPm(noteIdxPm(:,7+2*N) == reg);
		note.AllThresh = [note.AllThresh; noteRegime(powRegime > note.Thresholds(idxpm, reg), :)];
		note.powThresh = [note.powThresh; powRegime(powRegime > note.Thresholds(idxpm, reg))];
	end
end

pmmuafaSol=unique(note.AllThresh(:,1:3),'rows');
if option.Workers>1
	SubBatch=floor(size(pmmuafaSol,1)/option.Workers);
	for i=1:option.Workers
		if i==option.Workers %to tackle the problem when the length is not a multiple.
			temp{i}=pmmuafaSol((i-1)*SubBatch+1:end,:);
		else
			temp{i}=pmmuafaSol((i-1)*SubBatch+1:i*SubBatch,:);
		end
	end
	pmmuafaSol2Process=temp;
end

if option.Workers>1
	parfor j=1:option.Workers %divide to conquer
		idxBatch{j} = zeros(size(note.AllThresh(:,1)));
		for i=1:size(pmmuafaSol2Process{j},1) %taking away the multisolutions
			idxInAllnotes = not(any(bsxfun(@minus,note.AllThresh(:,1:3),...
				pmmuafaSol2Process{j}(i,:)),2)); %hyper rapide.
			if sum(idxInAllnotes)>=1
				[~,idxBestnote]=max(note.powThresh(idxInAllnotes));%the note with the highest power
				idxActiv=find(idxInAllnotes);
				idxInAllnotes(idxActiv(idxBestnote))=0;
			end
			idxBatch{j} = idxBatch{j} + idxInAllnotes;
		end
	end
	idxBatchs = cell2mat(idxBatch);
	idxBatchs = sum(idxBatchs,2);
	idxBatchs = logical(idxBatchs);
	note.AllThreshCleaned = note.AllThresh;
	if sum(idxBatchs)>=1
		note.AllThreshCleaned(idxBatchs,:)=[];%removing the lines
	end
else
	%taking away the multisolutions (the multicore solution is 10 times faster...
	%if it works consistently, I will implement it also in the non multi-core
	%case).
	note.AllThreshCleaned = note.AllThresh;
	for i=1:size(pmmuafaSol,1)
		idxInAllnotes = not(any(bsxfun(@minus,note.AllThresh(:,1:3), pmmuafaSol(i,:)),2)); %very fast
		if sum(idxInAllnotes)>=1
			[~,idxBestnote]=max(note.powThresh(idxInAllnotes));%the note with the highest power
			idxActiv=find(idxInAllnotes);
			idxInAllnotes(idxActiv(idxBestnote))=0;
			note.AllThreshCleaned(idxInAllnotes,:)=[];%removing the line
		end
	end
end
try
	note.AllThreshCleanedPow = note.AllThreshCleaned(:,5:N+5)+1i*note.AllThreshCleaned(:,N+6:2*N+6);
catch
	for idxpm = 1:size(param.Pm2keep, 1)
		note.taken{idxpm} = [];
		note.powTaken{idxpm} = [];
	end
	return
end
note.AllThreshCleanedPow = sqrt(sum(abs(note.AllThreshCleanedPow).^2,2));
note.AllThreshCleanedPow=note.AllThreshCleanedPow./note.AllThreshCleaned(:,1);

save([param.pathResults '/Protection/ProtectingCleaningDoubleSolAfterRaw' clockFormat],...
	'note', 'param', 'option');

if ~isfield(note,'taken')
	note.taken{idxpm} = [];
	note.powTaken{idxpm} = [];
end
for idxpm = 1:size(param.Pm2keep, 1)
	noteIdxPm = note.AllThreshCleaned(note.AllThreshCleaned(:, 1) > param.Pm2keep(idxpm, 1) &...
		note.AllThreshCleaned(:, 1) < param.Pm2keep(idxpm, 2), :);
	powIdxPm = note.AllThreshCleanedPow(note.AllThreshCleaned(:, 1) > param.Pm2keep(idxpm, 1) &...
		note.AllThreshCleaned(:, 1) < param.Pm2keep(idxpm, 2));
	for reg = param.Regimes
		note.taken{idxpm} = [noteIdxPm(noteIdxPm(:,7+2*N) == reg, :); note.taken{idxpm}];
		note.powTaken{idxpm} = [powIdxPm(noteIdxPm(:,7+2*N) == reg); note.powTaken{idxpm}];
	end
end
end
