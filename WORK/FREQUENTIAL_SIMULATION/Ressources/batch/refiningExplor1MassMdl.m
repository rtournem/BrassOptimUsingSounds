function [note,param]=refiningExplor1MassMdl(param,note,option)
%improving the zone definition by doing the exploration of the selected
%space. we use an hypercube circumscribing the zone

for idxpm=1:size(param.Pm2keep,1)
	for reg=param.Regimes
		paramtemp{reg}.type=param.type;
		paramtemp{reg}.N=param.N;
		idxReg{reg}= note.taken{idxpm}(:,end-size(param.xs,1))==reg;
		
		paramtemp{reg}.NbPts2Sim=param.Points2SampleRefin;
		paramtemp{reg}.muas=[min(note.taken{idxpm}(idxReg{reg},2)) max(note.taken{idxpm}(idxReg{reg},2))];
		% 		paramtemp{reg}.pms=[min(note.taken{idxpm}(idxReg{reg},1)) max(note.taken{idxpm}(idxReg{reg},1))];
		paramtemp{reg}.Pm2keep = param.Pm2keep;
		paramtemp{reg}.fas=[min(note.taken{idxpm}(idxReg{reg},3)) max(note.taken{idxpm}(idxReg{reg},3))];
		paramtemp{reg}.xs=[];
		paramtemp{reg}.Regimes = param.Regimes;
		paramtemp{reg}.pathResults = param.pathResults;
		
		rangeXs = diff(param.xs,1,2);
		for i=1:size(param.xs,1)
			%We take the min of the found note + a little radius in order to be less
			%harsh on design space reduction. a tenth of the range of a design
			%variable
			xsMin = max([param.xs(i,1), min(note.taken{idxpm}(idxReg{reg},...
				end - size(param.xs, 1) + i)) - rangeXs(i)/10]);
			xsMax = min([param.xs(i,2), max(note.taken{idxpm}(idxReg{reg},...
				end - size(param.xs, 1) + i)) + rangeXs(i)/10]);
			paramtemp{reg}.xs=[paramtemp{reg}.xs;
				min(note.taken{idxpm}(idxReg{reg}, end - size(param.xs, 1) + i)),...
				max(note.taken{idxpm}(idxReg{reg}, end - size(param.xs, 1) + i))];
		end
	end
	
	if option.Workers>1
		poolSimu=gcp('nocreate');%if no worker opened.
		if isempty(poolSimu)
			parpool(Option.Workers);
		elseif poolSimu.NumWorkers~=option.Workers %if the number of opened worker is not right.
			delete(gcp('nocreate'))
			parpool(option.Workers);
		end
		tic;
		param.verb=0;
		for reg=param.Regimes
			noteNeigh{reg}.Thresholds = note.Thresholds;
			[noteNeigh{reg},paramtemp{reg}]=range2Notes(paramtemp{reg}, noteNeigh{reg}, option);
		end
		
	else
		for reg=param.Regimes
			noteNeigh{reg}.Thresholds = note.Thresholds;
			[noteNeigh{reg},paramtemp{reg}] = range2Notes(paramtemp{reg}, noteNeigh{reg}, option);
		end
	end
	
	for reg=param.Regimes
		[noteNeigh{reg}, paramtemp{reg}] = cleaningNotes(paramtemp{reg}, noteNeigh{reg}, option);
		save([param.pathResults '/Protection/' 'notes_' param.type clockFormat...
			'_refined_idxpm_' num2str(idxpm) '_reg_' num2str(reg)], 'param','note','option');
		noteIdxPm = noteNeigh{reg}.AllThreshCleaned(noteNeigh{reg}.AllThreshCleaned(:, 1) > param.Pm2keep(idxpm, 1) &...
			noteNeigh{reg}.AllThreshCleaned(:, 1) < param.Pm2keep(idxpm, 2), :);
		powIdxPm = noteNeigh{reg}.AllThreshCleanedPow(noteNeigh{reg}.AllThreshCleaned(:, 1) > param.Pm2keep(idxpm, 1) &...
			noteNeigh{reg}.AllThreshCleaned(:, 1) < param.Pm2keep(idxpm, 2));
		note.taken{idxpm} = [noteIdxPm(noteIdxPm(:,7+2*param.N) == reg, :); note.taken{idxpm}];
		note.powTaken{idxpm} = [powIdxPm(noteIdxPm(:,7+2*param.N) == reg); note.powTaken{idxpm}];
	end
end
end
