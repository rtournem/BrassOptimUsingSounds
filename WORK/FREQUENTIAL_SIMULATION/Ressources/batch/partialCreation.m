function Pis=partialCreation(Musician,Finits,N,strat)
%Empirical recipe to create initial vector for the harmonic balance technique.

Pis=[];
if strcmp('OnlyFirstPartial',strat{1})
	%first try of the first partial + a little kick on the second one then only
	%a first partial
	Pis=zeros(length(Finits),2*N+2);
	%Adding some possible final frequencies to the signal
	Pis(:,N+3)=Finits;
	%putting some energy on the first harmonic
	Pis(:,2)=Musician(1)/2;
end
if strcmp('FirstAndSecondPartial',strat{1})
	Pistemp=zeros(length(Finits),2*N+2);
	Pistemp(:,N+3)=Finits;

%putting some energy on the first harmonic
Pistemp(:,2)=Musician(1)/2;
%unbalancing the second harmonic just a tiny bit
ratio=-1/10;
% 			ratio=-(1/6000)*(Musician(3)-200)-1/10;
Pistemp(1:length(Finits),3)=Pistemp(1:length(Finits),2)*ratio;
if ismember('Closer2reality',strat{1})
	% yto fit more the final sol you can add this component on the imaginary
	% partof the first partial.
	Pistemp(1:length(Finits),N+4)=Pistemp(1:length(Finits),2)*(-ratio/2);
end
Pis=[Pis;Pistemp];
end
end
