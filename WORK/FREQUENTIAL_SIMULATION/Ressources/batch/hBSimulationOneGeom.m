function soundsGeom=hBSimulationOneGeom(pmmuafaxs,param,nfo,varargin)
%like hBSimulation but for only one instrument.

%possibility to add sols in the initial sols vector
if (nargin>=4) SpecialSol=varargin{1}; else SpecialSol=[]; end
success=zeros(size(pmmuafaxs,1),1);
N=param.N;
%Notes contains on each line the mask,  the associated solution and the geometrical parameters.
soundsGeom=[];

%%                          IMPEDANCE
nfo.imped=impedPreProcess(2,param.type,pmmuafaxs(1,4:end),nfo.imped);
try
	[nfo.Instru.imped,fPeaks,holes]=zCreation(nfo.imped);
catch
	error('this candidate did not work');
end

%%                        BOUCLE  P=g(mask,Pi,Z)
for i=1:size(pmmuafaxs,1)
	mask=pmmuafaxs(i,1:3);
	disp(num2str(pmmuafaxs(i,:)));
	
	%%                      VECTEUR INITIAL
	Pis=initVectHB(mask,pmmuafaxs(i,4:end),[SpecialSol; soundsGeom],fPeaks,holes,param);
	if ~isfield(param,'verb')%field verb set at about line 40 of Range2Notes.m
		disp([num2str(mask(1)) '_' num2str(mask(2)) '_' num2str(mask(3))]);
		disp(['The number of initial vectors is: ' num2str(size(Pis,1))]);
	end
	%%                      BOUCLE  P=h(Pis)
	for j=1:size(Pis,1)
		nfo.Simu.Pi=Pis(j,:);
		nfo.mask.pm=mask(1);
		nfo.mask.mua=mask(2);
		nfo.mask.fa=mask(3);
		[isGood,~,Pfinal,nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim]=...
			eqHarmo(nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim);
		if isGood
			reg=find(holes<=Pfinal(N+3),1,'last'); %to see if I keep it
			if ~isfield(param,'verb')
				disp(['fPi: ' num2str(Pis(j,N+3)) ' fPf: ' num2str(Pfinal(N+3))]);
				if reg==find(Pfinal(N+3)>=fPeaks,1,'last');
					disp('convergence above resonnance peak');
				else
					disp('convergence under peak');
				end
			end
			success(i)=1;
			if param.RadXP
				Pext=stf(Pfinal,Pfinal(N+3),N,nfo.Instru.XpDir);
				Pfinal=[Pext(1:N+2) Pfinal(N+3) Pext(N+4:end)];
			end
			if isempty(soundsGeom)
				soundsGeom=[soundsGeom; mask, Pfinal(N+3), Pfinal, reg, pmmuafaxs(i,4:end)];
				soundsGeom(end,N+7)=0;
			else
				%if the mask did not converge yet
				if isempty(find(ismember(soundsGeom(:,1:3),mask,'rows'), 1))
					soundsGeom=[soundsGeom; mask, Pfinal(N+3), Pfinal , reg, pmmuafaxs(i,4:end)];
					soundsGeom(end,N+7)=0;
				else
					%if he did not converge yet towards this special note (discrimination
					%thanks to fPlay)
					if isempty(find(abs(soundsGeom(ismember(soundsGeom(:,1:3),mask,'rows'),4)-Pfinal(N+3))<10^-3, 1))
						soundsGeom=[soundsGeom; mask, Pfinal(N+3), Pfinal , reg, pmmuafaxs(i,4:end)];
						soundsGeom(end,N+7)=0;
					end
				end
			end
			%when we have 2 solutions in the lowest regime (general case is
			%2 solution in the lowest regime and one in superior regime)
			%then we stop the search cause' we have all we need. (speed
			%objective).
			lastNotes = (size(soundsGeom,1)-2 : size(soundsGeom,1));
			lastNotes(lastNotes<=0) = []; %if soundsGeom is too small
			lastNotes(~ismember(soundsGeom(end-(length(lastNotes)-1):end,1:3),mask,'rows')) = [];
			MusiFoundFreqs = soundsGeom(lastNotes,4);
			TargetReg=find(holes-min(MusiFoundFreqs)>0,1,'first')-1;
			if ~isempty(TargetReg)
				if length(find(fPeaks(TargetReg)-10<MusiFoundFreqs & MusiFoundFreqs<holes(TargetReg+1)))==2
					break;
				end
			end
		end
	end
end

end
