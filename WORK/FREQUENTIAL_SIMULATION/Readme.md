# General considerations

This folder contains every element needed to achieve the preprocessing
of the design problem. One script in this folder launches every
preprocessing step: `batchEH.m`. The code should be commented enough
to make it work. Consequently, I will only recall here the concepts.

This preprocessing is made of 5 steps:
1. test of the design space,
2. obtention of the "power threshold" for every dynamics range and
   every note,
3. coarse exploration of the entire parameters space ($`P_m`$, $`\mu_a`$, $`f_a`$ +
   geometrical variables),
4. refined exploration of the viable subsets,
5. estimation of the parameters space distributions, using an
   empirical rule based on a distance threshold.
   
> *Although $`P_m`$ stands logically for pressure in the mouth,
> $`\mu_a`$ and $`f_a`$ is not a clever choice. Indeed, it should be
> $`\mu_l`$, $`f_l`$ because they are related to the lips of the
> musician. I blindly followed one set of notation at the beginning of
> my thesis, and unfortunately contaminated my entire code with
> it. Consequently, I will stick to $`\mu_a`$ and $`f_a`$.*

At the end of the preprocessing, `batchEH.m` exports the results to
the optimization algorithm.

### Summary of the approach

The aim of the test of the design space is to understand if the set of
considered instruments (the design space) will be able to generate the
required notes. These tests let you define prior to the optimization,
interesting limits for the geometrical variables you want to
optimize. The "power" threshold is used to know if the sound that you
simulate can be deemed "musical", meaning possibly played by a
musician. We determine the "power" threshold manually studying the
simulations obtained from a standard geometry, for each note and each
dynamics range. The entire parameters space contains the 3 dimensions
representing the action of the musician on the instrument (the
pressure in the mouth $`P_m`$, the mass per area of the lips
$`\mu_a`$, and the resonnance frequency of the lips $`f_a`$) and the
geometrical variables you want to optimize. In order to make the
optimization possible using reasonable processing ressources, the
preprocessing will tell the computer the subsets of the entire
parameters space which will produce the required notes above the
"power" threshold. At least, there are as many subsets as notes to
produce and dynamics range to consider. A note in this context is a
musical note (C3, E4), and a dynamics range would be a range of
$`P_m`$ ($`pp`$, $`mp`$, $`ff`$). In order to realize this task, two
explorations of the entire parameters space using a Monte Carlo method
are required. A coarse exploration to get approximately the limit of
each subset, and a refined exploration to determine more accurately
the subsets. After these explorations the subsets are technically
defined as point clouds in the parameters space. These point clouds
can be seen as discrete representation of distributions. The limits of
these distributions are obtained empirically using a distance
threshold above which a point is considered out of the distribution.

Thanks to the preprocessing, for a given instrument, dynamics range
and note, the optimization algorithm will notably be able to propose
triplets ($`P_m`$, $`\mu_a`$, $`f_a`$) that will simulate sounds
verifying the corresponding "power threshold". These triplets are
randomly chosen inside the distributions identified by the
preprocessing.

We now detail each step.

# Test of the design space

Usually the user will set, based on his personnal experience, the
range of possible variations for each geometrical variable of the
instrument he wants to optimize. In the general case, this will
generate a design space which is a hypercube of $`\mathbb{R}^n`$,
$`n`$ being the number of geometrical variables to
optimize. Unfortunately, it is very likely that some extreme locations
in this hypercube, which we could name experimental geometries, may
completely fail to generate the required notes. This stage let you
test some specific geometries of your design space in order to refine
the ranges of the geometrical variables variation. In the end, the
explorations (step 3 and 4) will eventually find the subset of the
hypercube which can produce the required notes at the required
dynamics. This test phase helps to speed up the preprocessing and
optimization. In addition, I strongly recommend to devote a lot of
time in this step, because it helps understand which sort of
geometries are viable or not.

You will find in `batchEH.m`, like for every other step, the
corresponding parameters and triggers.

# "Power" threshold computation

It is quite easy to make a sound blowing in any wind
instrument. Unfortunately, if you are not trained, the sound will
certainly be deemed "not musical". The same fact is true for our
simulations. Consequently, we need a "musicality" descriptor based on
the generated sound to assess its musicality. In this context
musicality means the fact that it could have been produced by a
musician (a trained individual). In this work, a sound is deemed
musical if:

```math
\frac{\sqrt{\sum_{i=0}^{N}{P_i^2}}}{P_m} > \alpha \; ,

```

where $`P_i`$ is the intensity of the $`i`$th harmonic of the
generated sound.  $`\alpha`$ is called the "power" threshold since the
descriptor compares a norm of the generated harmonics intensity to the
pressure source $`P_m`$.

This step of the preprocessing aims to determine for each note and
each dynamics range the corresponding $`\alpha`$. In order to achieve
this task, a standard instrument of the design space (close to an
existing one for example) is studied. The space defined by the three
variables representing the action of the musician ($`P_m`$, $`\mu_a`$,
$`f_a`$), named hereafter the musician space, is sampled by a latin
hypercube (LHS). The harmonic balance technique tries to generate a
sound from every sample. The "musicality" descriptor is calculated for
every generated sound.

In order to find the "power" thresholds, we study the playing
frequency variance of every sound whose "musicality" descriptor is
above a given value. We observe that the playing frequency variance
diminishes with the "musicality" descriptor value, which is logical:
sounds having a good "musicality" descriptor value play reliably "in
tune", and vice versa.

The "power" threshold is the "musicality" descriptor value that
produces a playing frequency variance corresponding to live recordings
(around 3 cents depending on the note). Unfortunately, this is still
not currently automated, because the playing frequency variance
doesn't diminish monotonically with the "musicality" descriptor
value. This can be due to the fact that the higher the "musicality"
descriptor value, the lower the number of samples.  Consequenlty, the
playing frequency variance cannot be reliably computed for the highest
"musicality" descriptor values in general. Currently for this step of
the preprocessing, the user defines manually the "power" threshold,
looking at the curves showing the dependance between the playing
frequency variance and the values of the "musicality" descriptor. He
does this manual selection for each note and each dynamics range.

# Coarse exploration of the entire parameters space

Once the "power" threshold defined, the entire parameter space is
explored in order to find the subset of this space that generate
"musical" sounds, for each note and dynamics range, named hereafter
viable subsets. A first coarse exploration is undertaken doing a LHS
of the entire parameter space. The harmonic balance technique tries to
generate a sound from every sample. If the "musicality" descriptor of
the generated sound is above the "power" threshold it is stored with
its parameters, otherwise it is discarded. Few ressources have to be
spent for this step which only tries to identify the locations of the
space generating "musical" notes.

# Refined exploration of the viable subsets

After the coarse exploration, a refined exploration is undertaken for
every note and every dynamics range of interest. As before, a LHS is
done of the hypercube circumscribed to the viable subsets, and the
harmonic balance technique simulates every sample. The more refined
the exploration, the easier the optimization process. Indeed, this
refined exploration will let you have a discrete representation of the
distributions in the entire parameter space (point clouds), for each
note and each dynamics range. This will then largely help the computer
to know how to play the instruments of your design problem.

# Distribution limit estimation using a distance threshold

The refined exploration only provides a discrete representation of the
distribution of each viable subset, as point clouds. A strategy is
needed to help the computer pick arbitrarily samples in these
distributions. The strategy is based on a threshold distance above
which a point of the entire parameter space is considered too far from
the distibution to be part of it.

This distance we study is the distance of the incumbent point to its
Xth neighbor in the corresponding point cloud (we use the 4th neighbor
in our study). We did not use the 1st neighbor because of possible
outliers in the point cloud. We leave the possibility for the user to
choose the neighbor defining this distance. In order to find the
threshold for a given point cloud, we compute this distance for every
point of the cloud and choose arbitrarily a value close to the maximal
observed distance. We don't take the maximal distance for the same
reason regarding potential outliers.

In the code, boxplots help you understand what could be the best
neighbor index and value you could take for the distance threshold.
